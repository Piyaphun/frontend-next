import Router from 'next/router'
import cookie from "js-cookie";

const Logout = () => {
    cookie.remove("username");
    cookie.remove("token");
    Router.push({
      pathname: '/index'
    })
  }
const Navbar = () => (
    <body>
  <ul>
  <div><button  onClick={(props) =>Logout()} type="button">logout</button></div>
</ul>
  
  <style jsx>{`
  ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
    width:100%;
  }
  
  li {
    float: left;
  }
  
  li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
  }
  
  li a:hover {
    background-color: #111;
  }
  div{
      float : right;
  }
  div button {
    font-family: "Roboto", sans-serif;
    text-transdiv: uppercase;
    outline: 0;
    background: #4CAF50;

    border: 0;
   
    border-radius: 12px;
    padding: 15px;
    margin-left:10px;
    margin-top:10px;
    margin-right:10px;
    margin-bottom:10px;
    color: #FFFFFF;
    font-size: 14px;
    -webkit-transition: all 0.3 ease;
    transition: all 0.3 ease;
    cursor: pointer;
    re
  }
  li button:hover,.div button:active,.div button:focus {
    background: #43A047;
  }
  `}</style>
  </body>
)

export default Navbar
