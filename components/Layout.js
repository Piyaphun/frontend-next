import Header from './Header'
import Footer from './Footer'

const style = {
  background: '#76b852'
}
 
const Layout = (props) => (
  <body style={style}>
  <div style={style}>
    <Header />
    {props.children}
   
  </div>
  </body>
)

export default Layout