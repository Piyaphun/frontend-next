import Layout from '../components/Layout'
import Router from 'next/router'
import Link from 'next/link';
import fetch from 'isomorphic-unfetch';
import axios from 'axios';



const style = {
  background: '#76b852',
  width:'100%',
  hight:'100%'
}


const HandleSubmitsd = (props) => {
  props.preventDefault() // หยุดการทำงาน Submit
  const user = props.target.user.value
  const pass = props.target.pass.value

  axios.post(`http://localhost:7777/api/v0/user`,{
      username : user,
      password : pass
  })
  .then(res=>{
    alert("สมัครเรียบร้อยแล้ว");
      Router.push({
    pathname: '/index',
  })
  })
 
}

const Index = (props) => (
  <body style={style}>
    
  <Layout > 
  
    <form onSubmit={(props) => HandleSubmitsd(props)}>
    <h1>Register</h1>
      <input type="text" placeholder="username" name="user" required/>
      <input type="password" placeholder="password" name="pass" required/>
      <button type="submit">register</button>
      <div>
      <Link href = "/index"> 
            <button1>back</button1>
      </Link>
      </div>
    </form>

  </Layout>
  <style jsx>{`
      login-page {
        width: 360px;
        padding: 8% 0 0;
        margin: auto;
      }
    
      form div {
        margin-top: 20px;
        border: 1;
      }

      form button1 {
        border-radius: 12px;
        font-family: "Roboto", sans-serif;
        text-transform: uppercase;
        outline: 0;
        background: #8BA1E9;
        width: 100%;
        border: 0;
        padding: 15px;
        color: #FFFFFF;
        font-size: 14px;
        -webkit-transition: all 0.3 ease;
        transition: all 0.3 ease;
        cursor: pointer;
      }

      form {
        position: relative;
        z-index: 1;
        background: #FFFFFF;
        max-width: 360px;
        margin: 0 auto 100px;
        margin-top: 100px;
        padding: 45px;
        text-align: center;
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
      }
      
      form h {
        margin-top: 100px;
        border: 1;
      }

      form input {
        font-family: "Roboto", sans-serif;
        outline: 0;
        background: #f2f2f2;
        width: 100%;
        border: 0;
        margin: 0 0 15px;
        padding: 15px;
        box-sizing: border-box;
        font-size: 14px;
      }
      form button {
        font-family: "Roboto", sans-serif;
        text-transform: uppercase;
        outline: 0;
        background: #4CAF50;
        width: 100%;
        border-radius: 12px;
        border: 0;
        padding: 15px;
        color: #FFFFFF;
        font-size: 14px;
        -webkit-transition: all 0.3 ease;
        transition: all 0.3 ease;
        cursor: pointer;
      }
      form button:hover,.form button:active,.form button:focus {
        background: #43A047;
      }
      form .message {
        margin: 15px 0 0;
        color: #b3b3b3;
        font-size: 12px;
      }
      form .message a {
        color: #4CAF50;
        text-decoration: none;
      }
      form .register-form {
        display: none;
      }
      container {
        position: relative;
        z-index: 1;
        max-width: 300px;
        margin: 0 auto;
      }
      container:before, .container:after {
       content: "";
        display: block;
        clear: both;
      }
      container .info {
        margin: 50px auto;
        text-align: center;
      }
      container .info h1 {
        margin: 0 0 15px;
        padding: 0;
        font-size: 36px;
        font-weight: 300;
        color: #1a1a1a;
      }
      container .info span {
       color: #4d4d4d;
        font-size: 12px;
      }
      container .info span a {
        color: #000000;
        text-decoration: none;
      }
    container .info span .fa {
        color: #EF3B3A;
      }
      body {
        background: #76b852; /* fallback for old browsers */
        background: -webkit-linear-gradient(right, #76b852, #8DC26F);
        background: -moz-linear-gradient(right, #76b852, #8DC26F);
        background: -o-linear-gradient(right, #76b852, #8DC26F);
        background: linear-gradient(to left, #76b852, #8DC26F);
        font-family: "Roboto", sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;      
      }
      `}</style>
  </body>

)


export default Index
