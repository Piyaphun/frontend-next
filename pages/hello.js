import Layout from '../components/Layout'
import Navbar from '../components/Navbar'
import cookie from "js-cookie";
import Router from 'next/router'
import axios from 'axios'
import fetch from 'isomorphic-unfetch';
import Popup from "reactjs-popup";

  const editUser = (props) => {
    props.preventDefault() // หยุดการทำงาน Submi;
    const user = props.target.user.value
    const pass = props.target.pass.value
    const id = props.target.id.value
  
    axios.put(`http://localhost:7777/api/v0/user`,{
        username : user,
        password : pass,
        _id : id
    })
    .then(res=>{
      alert("แก้ไขเรียบร้อย");
      Router.push({
        pathname: '/hello'
      })
      })
   
  }
  
  const createUser = (props) => {
    props.preventDefault() // หยุดการทำงาน Submit
    const user = props.target.user.value
    const pass = props.target.pass.value
  
    axios.post(`http://localhost:7777/api/v0/user`,{
        username : user,
        password : pass
    })
    .then(res=>{
      alert("สมัครเรียบร้อยแล้ว");
        Router.push({
      pathname: '/hello'
    })
  
    props.onClose;
    })
   
  }

  const deleteUser = (props) => {
    props.preventDefault() // หยุดการทำงาน Submit
    const id=  props.target.id.value
    props.preventDefault() // หยุดการทำงาน Submit
 
    axios.delete(`http://localhost:7777/api/v0/user?_id=${id}`
    )
    .then(res=>{
      console.log(res);
      if(res.data.status==1){
      alert("ลบเรียบร้อย");
      Router.push({
        pathname: '/hello'
      })
      
    }
    })
   
  }


 
const Hello = props => (
  
  <body>
   <Navbar />
   
  <Layout> 
    <div>
    <Popup 
        trigger={<button className="button"> Create </button>} modal > 
         {close => (
        <form onSubmit={(props) => createUser(props)}>
          <h1>Create</h1>
          <input type="text"  placeholder="username" name="user" required/>
          <input type="text"  placeholder="password" name="pass" required/>
          <button type="submit">Create</button>
        </form>
         )}
    </Popup>
     <h1>{cookie.get("username")}</h1> 
       <table>
            <tr>
                <th>Usernmae</th>
                <th>Password</th>
                <th>Edit</th>
            </tr> 
            
      {props.data.map(data =>(
            <tr>
                <td>{data.username}</td>
                <td>{data.password}</td>
                <td> 
                  <td>
                    <Popup 
                      trigger={<button className="button"> Edit </button>} modal> 
                      
                      <form onSubmit={(props) => editUser(props)}>
                        <h1>Edit</h1>
                        <input type="text"  placeholder={data.username} name="user" required/>
                        <input type="text"  placeholder={data.password} name="pass" required/>
                        <input type="hidden"  value={data._id} name="id" required/>
                        <button type="submit">Edit</button>
                      </form>
                      
                    </Popup>
                  </td>
                  <td> 
                  <Popup 
                      trigger={<button className="button"> Delete </button>}> 
                      
                      <form onSubmit={(props) => deleteUser(props)}>
                          <h1>Delete ?</h1>
                        <input type="hidden"  value={data._id} name="id"/>
                        <button type="submit">Delete</button>
                      </form>
                     
                    </Popup>
                  </td>
                </td>
            </tr>
        ))}
  
   
        </table>
     
    </div>
  </Layout>
  
  <style jsx>{`

      h1 { text-align: center;}
      form button1 {
        border-radius: 12px;
        font-family: "Roboto", sans-serif;
        text-transform: uppercase;
        outline: 0;
        background: #8BA1E9;
        width: 100%;
        border: 0;
        padding: 15px;
        color: #FFFFFF;
        font-size: 14px;
        -webkit-transition: all 0.3 ease;
        transition: all 0.3 ease;
        cursor: pointer;
      }

      form {
        background: #FFFFFF;
        padding: 45px;
        text-align: center;
      }

      form h {
        margin-top: 100px;
        border: 1;
      }

      form input {
        font-family: "Roboto", sans-serif;
        outline: 0;
        background: #f2f2f2;
        width: 100%;
        border: 0;
        margin: 0 0 15px;
        padding: 15px;
        box-sizing: border-box;
        font-size: 14px;
      }
      form button {
        font-family: "Roboto", sans-serif;
        text-transform: uppercase;
        outline: 0;
        background: #4CAF50;
        width: 100%;
        border-radius: 12px;
        border: 0;
        padding: 15px;
        color: #FFFFFF;
        font-size: 14px;
        -webkit-transition: all 0.3 ease;
        transition: all 0.3 ease;
        cursor: pointer;
      }
      form button:hover,.form button:active,.form button:focus {
        background: #43A047;
      }
      form .message {
        margin: 15px 0 0;
        color: #b3b3b3;
        font-size: 12px;
      }
      form .message a {
        color: #4CAF50;
        text-decoration: none;
      }
      form .register-form {
        display: none;
      }

      modal {
        position: relative;
     
      padding: 5px;
      }
      modal > .header {
        width: 100%;
    
        font-size: 18px;
        text-align: center;
        padding: 5px;
      }
      modal > .content {
        width: 100%;
        padding: 10px 5px;
      }
      modal > .actions {
        width: 100%;
        padding: 10px 5px;
        margin: auto;
        text-align: center;
      }
    
      table {
        border-collapse: collapse;
        width: 100%;
      }
      
      th, td {
        text-align: left;
        padding: 10px;
      }
      
      tr:nth-child(even){background-color: #f2f2f2}
      
      th {
        background-color: #4CAF50;
        color: white;
      }
      
      login-page {
        width: 360px;
        padding: 8% 0 0;
        margin: auto;
      }
     
      div {
        position: relative;
        z-index: 1;
        background: #FFFFFF;
        max-width: 1000px;
        margin: 0 auto 0px;
       
        padding: 45px;
        text-align: center;
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
      }
      div h {
        margin: 20px;
        border: 1;
      }
      div input {
        font-family: "Roboto", sans-serif;
        outline: 0;
        background: #f2f2f2;
        width: 100%;
        border: 0;
        margin: 0 0 15px;
        padding: 15px;
        box-sizing: border-box;
        font-size: 14px;
      }
      div button {
        font-family: "Roboto", sans-serif;
        text-transdiv: uppercase;
        outline: 0;
        background: #4CAF50;
        border: 0;
        float : right;
        padding: 15px;
        color: #FFFFFF;
        font-size: 14px;
        -webkit-transition: all 0.3 ease;
        transition: all 0.3 ease;
        cursor: pointer;
      }
      div button:hover,.div button:active,.div button:focus {
        background: #43A047;
      }
      div .message {
        margin: 15px 0 0;
        color: #b3b3b3;
        font-size: 12px;
      }
      div .message a {
        color: #4CAF50;
        text-decoration: none;
      }
      div .register-div {
        display: none;
      }
      container {
        
        z-index: 1;
        max-width: 500px;
        margin: 0 auto;
      }
      container:before, .container:after {
       content: "";
        display: block;
        clear: both;
      }
      container .info {
        margin: 50px auto;
        text-align: center;
      }
      container .info h1 {
        margin: 0 0 15px;
        padding: 0;
        font-size: 36px;
        font-weight: 300;
        color: #1a1a1a;
      }
      container .info span {
       color: #4d4d4d;
        font-size: 12px;
      }
      container .info span a {
        color: #000000;
        text-decoration: none;
      }
    container .info span .fa {
        color: #EF3B3A;
      }
      body {
        font-family: "Roboto", sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;      
      }
      `}</style>
  </body>
);

Hello.getInitialProps = async function() {
  const res = await fetch('http://localhost:7777/api/v0//user')
  const data = await res.json();
  return {data};
}



export default Hello