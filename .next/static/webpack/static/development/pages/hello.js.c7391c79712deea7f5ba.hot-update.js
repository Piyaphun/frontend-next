webpackHotUpdate("static\\development\\pages\\hello.js",{

/***/ "./node_modules/isomorphic-unfetch/browser.js":
/*!****************************************************!*\
  !*** ./node_modules/isomorphic-unfetch/browser.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = window.fetch || (window.fetch = __webpack_require__(/*! unfetch */ "./node_modules/unfetch/dist/unfetch.mjs").default || __webpack_require__(/*! unfetch */ "./node_modules/unfetch/dist/unfetch.mjs"));


/***/ }),

/***/ "./pages/hello.js":
/*!************************!*\
  !*** ./pages/hello.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime-corejs2/helpers/esm/asyncToGenerator.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/Layout */ "./components/Layout.js");
/* harmony import */ var _components_Navbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/Navbar */ "./components/Navbar.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! js-cookie */ "./node_modules/js-cookie/src/js.cookie.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/isomorphic-unfetch/browser.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var reactjs_popup__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! reactjs-popup */ "./node_modules/reactjs-popup/reactjs-popup.es.js");


var _jsxFileName = "C:\\crud\\pages\\hello.js";


var __jsx = react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement;








var editUser = function editUser(props) {
  props.preventDefault(); // หยุดการทำงาน Submi;

  var user = props.target.user.value;
  var pass = props.target.pass.value;
  var id = props.target.id.value;
  axios__WEBPACK_IMPORTED_MODULE_8___default.a.put("http://localhost:7777/api/v0/user", {
    username: user,
    password: pass,
    _id: id
  }).then(function (res) {
    alert("แก้ไขเรียบร้อย");
    next_router__WEBPACK_IMPORTED_MODULE_7___default.a.push({
      pathname: '/hello'
    });
  });
};

var createUser = function createUser(props) {
  props.preventDefault(); // หยุดการทำงาน Submit

  var user = props.target.user.value;
  var pass = props.target.pass.value;
  axios__WEBPACK_IMPORTED_MODULE_8___default.a.post("http://localhost:7777/api/v0/user", {
    username: user,
    password: pass
  }).then(function (res) {
    alert("สมัครเรียบร้อยแล้ว");
    next_router__WEBPACK_IMPORTED_MODULE_7___default.a.push({
      pathname: '/hello'
    });
    props.onClose;
  });
};

var deleteUser = function deleteUser(props) {
  props.preventDefault(); // หยุดการทำงาน Submit

  var id = props.target.id.value;
  props.preventDefault(); // หยุดการทำงาน Submit

  axios__WEBPACK_IMPORTED_MODULE_8___default.a["delete"]("http://localhost:7777/api/v0/user?_id=".concat(id)).then(function (res) {
    console.log(res);

    if (res.data.status == 1) {
      alert("ลบเรียบร้อย");
      next_router__WEBPACK_IMPORTED_MODULE_7___default.a.push({
        pathname: '/hello'
      });
    }
  });
};

var Hello = function Hello(props) {
  return __jsx("body", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73
    },
    __self: this
  }, __jsx(_components_Navbar__WEBPACK_IMPORTED_MODULE_5__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74
    },
    __self: this
  }), __jsx(_components_Layout__WEBPACK_IMPORTED_MODULE_4__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76
    },
    __self: this
  }, __jsx("div", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77
    },
    __self: this
  }, __jsx(reactjs_popup__WEBPACK_IMPORTED_MODULE_10__["default"], {
    trigger: __jsx("button", {
      className: "jsx-3629234234" + " " + "button",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79
      },
      __self: this
    }, " Create "),
    modal: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 78
    },
    __self: this
  }, function (close) {
    return __jsx("form", {
      onSubmit: function onSubmit(props) {
        return createUser(props);
      },
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81
      },
      __self: this
    }, __jsx("h1", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82
      },
      __self: this
    }, "Create"), __jsx("input", {
      type: "text",
      placeholder: "username",
      name: "user",
      required: true,
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 83
      },
      __self: this
    }), __jsx("input", {
      type: "text",
      placeholder: "password",
      name: "pass",
      required: true,
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 84
      },
      __self: this
    }), __jsx("button", {
      type: "submit",
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85
      },
      __self: this
    }, "Create"));
  }), __jsx("h1", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 89
    },
    __self: this
  }, js_cookie__WEBPACK_IMPORTED_MODULE_6___default.a.get("username")), __jsx("table", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 90
    },
    __self: this
  }, __jsx("tr", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 91
    },
    __self: this
  }, __jsx("th", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 92
    },
    __self: this
  }, "Usernmae"), __jsx("th", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 93
    },
    __self: this
  }, "Password"), __jsx("th", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 94
    },
    __self: this
  }, "Edit")), props.data.map(function (data) {
    return __jsx("tr", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98
      },
      __self: this
    }, __jsx("td", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99
      },
      __self: this
    }, data.username), __jsx("td", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 100
      },
      __self: this
    }, data.password), __jsx("td", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 101
      },
      __self: this
    }, __jsx("td", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 102
      },
      __self: this
    }, __jsx(reactjs_popup__WEBPACK_IMPORTED_MODULE_10__["default"], {
      trigger: __jsx("button", {
        className: "jsx-3629234234" + " " + "button",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 104
        },
        __self: this
      }, " Edit "),
      modal: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 103
      },
      __self: this
    }, __jsx("form", {
      onSubmit: function onSubmit(props) {
        return editUser(props);
      },
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 106
      },
      __self: this
    }, __jsx("h1", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 107
      },
      __self: this
    }, "Edit"), __jsx("input", {
      type: "text",
      placeholder: data.username,
      name: "user",
      required: true,
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 108
      },
      __self: this
    }), __jsx("input", {
      type: "text",
      placeholder: data.password,
      name: "pass",
      required: true,
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 109
      },
      __self: this
    }), __jsx("input", {
      type: "hidden",
      value: data._id,
      name: "id",
      required: true,
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 110
      },
      __self: this
    }), __jsx("button", {
      type: "submit",
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 111
      },
      __self: this
    }, "Edit")))), __jsx("td", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 116
      },
      __self: this
    }, __jsx(reactjs_popup__WEBPACK_IMPORTED_MODULE_10__["default"], {
      trigger: __jsx("button", {
        className: "jsx-3629234234" + " " + "button",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 118
        },
        __self: this
      }, " Delete "),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 117
      },
      __self: this
    }, __jsx("form", {
      onSubmit: function onSubmit(props) {
        return deleteUser(props);
      },
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 120
      },
      __self: this
    }, __jsx("h1", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 121
      },
      __self: this
    }, "Delete ?"), __jsx("input", {
      type: "hidden",
      value: data._id,
      name: "id",
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 122
      },
      __self: this
    }), __jsx("button", {
      type: "submit",
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 123
      },
      __self: this
    }, "Delete"))))));
  })))), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "3629234234",
    __self: this
  }, "h1.jsx-3629234234{text-align:center;}form.jsx-3629234234 button1.jsx-3629234234{border-radius:12px;font-family:\"Roboto\",sans-serif;text-transform:uppercase;outline:0;background:#8BA1E9;width:100%;border:0;padding:15px;color:#FFFFFF;font-size:14px;-webkit-transition:all 0.3 ease;-webkit-transition:all 0.3 ease;transition:all 0.3 ease;cursor:pointer;}form.jsx-3629234234{background:#FFFFFF;padding:45px;text-align:center;}form.jsx-3629234234 h.jsx-3629234234{margin-top:100px;border:1;}form.jsx-3629234234 input.jsx-3629234234{font-family:\"Roboto\",sans-serif;outline:0;background:#f2f2f2;width:100%;border:0;margin:0 0 15px;padding:15px;box-sizing:border-box;font-size:14px;}form.jsx-3629234234 button.jsx-3629234234{font-family:\"Roboto\",sans-serif;text-transform:uppercase;outline:0;background:#4CAF50;width:100%;border-radius:12px;border:0;padding:15px;color:#FFFFFF;font-size:14px;-webkit-transition:all 0.3 ease;-webkit-transition:all 0.3 ease;transition:all 0.3 ease;cursor:pointer;}form.jsx-3629234234 button.jsx-3629234234:hover,.form.jsx-3629234234 button.jsx-3629234234:active,.form.jsx-3629234234 button.jsx-3629234234:focus{background:#43A047;}form.jsx-3629234234 .message.jsx-3629234234{margin:15px 0 0;color:#b3b3b3;font-size:12px;}form.jsx-3629234234 .message.jsx-3629234234 a.jsx-3629234234{color:#4CAF50;-webkit-text-decoration:none;text-decoration:none;}form.jsx-3629234234 .register-form.jsx-3629234234{display:none;}modal.jsx-3629234234{position:relative;padding:5px;}modal.jsx-3629234234>.header.jsx-3629234234{width:100%;font-size:18px;text-align:center;padding:5px;}modal.jsx-3629234234>.content.jsx-3629234234{width:100%;padding:10px 5px;}modal.jsx-3629234234>.actions.jsx-3629234234{width:100%;padding:10px 5px;margin:auto;text-align:center;}table.jsx-3629234234{border-collapse:collapse;width:100%;}th.jsx-3629234234,td.jsx-3629234234{text-align:left;padding:10px;}tr.jsx-3629234234:nth-child(even){background-color:#f2f2f2;}th.jsx-3629234234{background-color:#4CAF50;color:white;}login-page.jsx-3629234234{width:360px;padding:8% 0 0;margin:auto;}div.jsx-3629234234{position:relative;z-index:1;background:#FFFFFF;max-width:1000px;margin:0 auto 0px;padding:45px;text-align:center;box-shadow:0 0 20px 0 rgba(0,0,0,0.2),0 5px 5px 0 rgba(0,0,0,0.24);}div.jsx-3629234234 h.jsx-3629234234{margin:20px;border:1;}div.jsx-3629234234 input.jsx-3629234234{font-family:\"Roboto\",sans-serif;outline:0;background:#f2f2f2;width:100%;border:0;margin:0 0 15px;padding:15px;box-sizing:border-box;font-size:14px;}div.jsx-3629234234 button.jsx-3629234234{font-family:\"Roboto\",sans-serif;text-transdiv:uppercase;outline:0;background:#4CAF50;border:0;float :right;padding:15px;color:#FFFFFF;font-size:14px;-webkit-transition:all 0.3 ease;-webkit-transition:all 0.3 ease;transition:all 0.3 ease;cursor:pointer;}div.jsx-3629234234 button.jsx-3629234234:hover,.div.jsx-3629234234 button.jsx-3629234234:active,.div.jsx-3629234234 button.jsx-3629234234:focus{background:#43A047;}div.jsx-3629234234 .message.jsx-3629234234{margin:15px 0 0;color:#b3b3b3;font-size:12px;}div.jsx-3629234234 .message.jsx-3629234234 a.jsx-3629234234{color:#4CAF50;-webkit-text-decoration:none;text-decoration:none;}div.jsx-3629234234 .register-div.jsx-3629234234{display:none;}container.jsx-3629234234{z-index:1;max-width:500px;margin:0 auto;}container.jsx-3629234234:before,.container.jsx-3629234234:after{content:\"\";display:block;clear:both;}container.jsx-3629234234 .info.jsx-3629234234{margin:50px auto;text-align:center;}container.jsx-3629234234 .info.jsx-3629234234 h1.jsx-3629234234{margin:0 0 15px;padding:0;font-size:36px;font-weight:300;color:#1a1a1a;}container.jsx-3629234234 .info.jsx-3629234234 span.jsx-3629234234{color:#4d4d4d;font-size:12px;}container.jsx-3629234234 .info.jsx-3629234234 span.jsx-3629234234 a.jsx-3629234234{color:#000000;-webkit-text-decoration:none;text-decoration:none;}container.jsx-3629234234 .info.jsx-3629234234 span.jsx-3629234234 .fa.jsx-3629234234{color:#EF3B3A;}body.jsx-3629234234{font-family:\"Roboto\",sans-serif;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxjcnVkXFxwYWdlc1xcaGVsbG8uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBeUljLEFBRzZCLEFBRUQsQUFnQkEsQUFNRixBQUtnQixBQVdBLEFBZWQsQUFHSCxBQUtGLEFBSUQsQUFJSyxBQUtQLEFBT0EsQUFJQSxBQU9jLEFBS1QsQUFJd0IsQUFHZixBQUtiLEFBTU0sQUFXTixBQUlxQixBQVdBLEFBY2QsQUFHSCxBQUtGLEFBSUQsQUFJSCxBQUtBLEFBS08sQUFJRCxBQU9ILEFBSUMsQUFJQSxBQUdtQixVQS9CakIsQ0FoR0QsQUFNRSxBQUlBLEFBMkZILENBbkVDLEFBaUJOLENBN0RYLEFBc0dBLENBMUd1QixBQXNHQSxBQTZCTixBQUlNLEFBSXZCLEVBaEpnQixBQXlDRCxBQTZEQyxBQTJCSixDQW5LRCxBQStKUyxDQXhMRyxBQXlFWCxBQTZDQSxDQW5IdUIsQUFnQnBCLEFBcUNmLEFBc0dBLEVBNUJBLElBbkNhLEFBUThCLEFBSTdCLEFBeUVELENBMUpiLEFBcURvQixBQWdHSixBQWNDLENBNUVILENBNUJkLEFBSWMsQUE4Qk8sQ0FsQnJCLEFBK0ZBLENBeElpQixBQWNqQixBQXdGaUIsRUE5SUcsQUFVUixBQVdlLEFBNEZmLEFBV2MsQUE4RFcsR0FyQnJDLENBekZBLEFBcUZBLENBekVBLEVBTUEsQ0F4Qm9CLEFBc0ZwQixDQWNrQixDQS9KRyxBQXVHQSxFQXREUCxDQW5CZCxBQXNHQSxFQTNDbUIsR0FuR25CLENBakIyQixLQTZFM0IsQUFnRVksQ0F2R0EsQUFxSkksQ0FwR2hCLEdBM0RhLEFBdUdBLEdBdEViLEFBd0RvQixBQThDcEIsQUFpQ0EsRUF0RHFCLENBdkdBLEFBb0tlLElBZnBDLENBL0pXLEFBdUdBLElBbklDLEtBNkJNLEFBdUdBLENBZEgsR0F3QkosQ0E3SVUsQUFzQ1IsUUF3R0UsQ0F4QkssRUF6RkwsQUFVTSxBQTZGTixJQXNFZixJQXpNYSxFQThJRSxHQWpIUyxBQXVHQSxHQWRvRCxHQXJIakUsQUFzQ0EsSUF3R0ssS0E3SUQsQUFzQ0EsT0FWRSxBQXVHQSxFQVdBLElBN0lELEFBc0NBLFNBVmhCLEFBdUdBLEVBV2tDLEdBN0lqQixBQXNDQSxlQXJDaUIsQUFzQ0EsYUE0RWxDLENBMkIwQixrQkE1SUEsQUFzQ0Esc0NBdUdULGVBQ2pCLEdBN0lpQixBQXNDQSxlQXJDakIsQUFzQ0EiLCJmaWxlIjoiQzpcXGNydWRcXHBhZ2VzXFxoZWxsby5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBMYXlvdXQgZnJvbSAnLi4vY29tcG9uZW50cy9MYXlvdXQnXHJcbmltcG9ydCBOYXZiYXIgZnJvbSAnLi4vY29tcG9uZW50cy9OYXZiYXInXHJcbmltcG9ydCBjb29raWUgZnJvbSBcImpzLWNvb2tpZVwiO1xyXG5pbXBvcnQgUm91dGVyIGZyb20gJ25leHQvcm91dGVyJ1xyXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnXHJcbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLXVuZmV0Y2gnO1xyXG5pbXBvcnQgUG9wdXAgZnJvbSBcInJlYWN0anMtcG9wdXBcIjtcclxuXHJcbiAgY29uc3QgZWRpdFVzZXIgPSAocHJvcHMpID0+IHtcclxuICAgIHByb3BzLnByZXZlbnREZWZhdWx0KCkgLy8g4Lir4Lii4Li44LiU4LiB4Liy4Lij4LiX4Liz4LiH4Liy4LiZIFN1Ym1pO1xyXG4gICAgY29uc3QgdXNlciA9IHByb3BzLnRhcmdldC51c2VyLnZhbHVlXHJcbiAgICBjb25zdCBwYXNzID0gcHJvcHMudGFyZ2V0LnBhc3MudmFsdWVcclxuICAgIGNvbnN0IGlkID0gcHJvcHMudGFyZ2V0LmlkLnZhbHVlXHJcbiAgXHJcbiAgICBheGlvcy5wdXQoYGh0dHA6Ly9sb2NhbGhvc3Q6Nzc3Ny9hcGkvdjAvdXNlcmAse1xyXG4gICAgICAgIHVzZXJuYW1lIDogdXNlcixcclxuICAgICAgICBwYXNzd29yZCA6IHBhc3MsXHJcbiAgICAgICAgX2lkIDogaWRcclxuICAgIH0pXHJcbiAgICAudGhlbihyZXM9PntcclxuICAgICAgYWxlcnQoXCLguYHguIHguYnguYTguILguYDguKPguLXguKLguJrguKPguYnguK3guKJcIik7XHJcbiAgICAgIFJvdXRlci5wdXNoKHtcclxuICAgICAgICBwYXRobmFtZTogJy9oZWxsbydcclxuICAgICAgfSlcclxuICAgICAgfSlcclxuICAgXHJcbiAgfVxyXG4gIFxyXG4gIGNvbnN0IGNyZWF0ZVVzZXIgPSAocHJvcHMpID0+IHtcclxuICAgIHByb3BzLnByZXZlbnREZWZhdWx0KCkgLy8g4Lir4Lii4Li44LiU4LiB4Liy4Lij4LiX4Liz4LiH4Liy4LiZIFN1Ym1pdFxyXG4gICAgY29uc3QgdXNlciA9IHByb3BzLnRhcmdldC51c2VyLnZhbHVlXHJcbiAgICBjb25zdCBwYXNzID0gcHJvcHMudGFyZ2V0LnBhc3MudmFsdWVcclxuICBcclxuICAgIGF4aW9zLnBvc3QoYGh0dHA6Ly9sb2NhbGhvc3Q6Nzc3Ny9hcGkvdjAvdXNlcmAse1xyXG4gICAgICAgIHVzZXJuYW1lIDogdXNlcixcclxuICAgICAgICBwYXNzd29yZCA6IHBhc3NcclxuICAgIH0pXHJcbiAgICAudGhlbihyZXM9PntcclxuICAgICAgYWxlcnQoXCLguKrguKHguLHguITguKPguYDguKPguLXguKLguJrguKPguYnguK3guKLguYHguKXguYnguKdcIik7XHJcbiAgICAgICAgUm91dGVyLnB1c2goe1xyXG4gICAgICBwYXRobmFtZTogJy9oZWxsbydcclxuICAgIH0pXHJcbiAgXHJcbiAgICBwcm9wcy5vbkNsb3NlO1xyXG4gICAgfSlcclxuICAgXHJcbiAgfVxyXG5cclxuICBjb25zdCBkZWxldGVVc2VyID0gKHByb3BzKSA9PiB7XHJcbiAgICBwcm9wcy5wcmV2ZW50RGVmYXVsdCgpIC8vIOC4q+C4ouC4uOC4lOC4geC4suC4o+C4l+C4s+C4h+C4suC4mSBTdWJtaXRcclxuICAgIGNvbnN0IGlkPSAgcHJvcHMudGFyZ2V0LmlkLnZhbHVlXHJcbiAgICBwcm9wcy5wcmV2ZW50RGVmYXVsdCgpIC8vIOC4q+C4ouC4uOC4lOC4geC4suC4o+C4l+C4s+C4h+C4suC4mSBTdWJtaXRcclxuIFxyXG4gICAgYXhpb3MuZGVsZXRlKGBodHRwOi8vbG9jYWxob3N0Ojc3NzcvYXBpL3YwL3VzZXI/X2lkPSR7aWR9YFxyXG4gICAgKVxyXG4gICAgLnRoZW4ocmVzPT57XHJcbiAgICAgIGNvbnNvbGUubG9nKHJlcyk7XHJcbiAgICAgIGlmKHJlcy5kYXRhLnN0YXR1cz09MSl7XHJcbiAgICAgIGFsZXJ0KFwi4Lil4Lia4LmA4Lij4Li14Lii4Lia4Lij4LmJ4Lit4LiiXCIpO1xyXG4gICAgICBSb3V0ZXIucHVzaCh7XHJcbiAgICAgICAgcGF0aG5hbWU6ICcvaGVsbG8nXHJcbiAgICAgIH0pXHJcbiAgICAgIFxyXG4gICAgfVxyXG4gICAgfSlcclxuICAgXHJcbiAgfVxyXG5cclxuXHJcbiBcclxuY29uc3QgSGVsbG8gPSBwcm9wcyA9PiAoXHJcbiAgXHJcbiAgPGJvZHk+XHJcbiAgIDxOYXZiYXIgLz5cclxuICAgXHJcbiAgPExheW91dD4gXHJcbiAgICA8ZGl2PlxyXG4gICAgPFBvcHVwIFxyXG4gICAgICAgIHRyaWdnZXI9ezxidXR0b24gY2xhc3NOYW1lPVwiYnV0dG9uXCI+IENyZWF0ZSA8L2J1dHRvbj59IG1vZGFsID4gXHJcbiAgICAgICAgIHtjbG9zZSA9PiAoXHJcbiAgICAgICAgPGZvcm0gb25TdWJtaXQ9eyhwcm9wcykgPT4gY3JlYXRlVXNlcihwcm9wcyl9PlxyXG4gICAgICAgICAgPGgxPkNyZWF0ZTwvaDE+XHJcbiAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiAgcGxhY2Vob2xkZXI9XCJ1c2VybmFtZVwiIG5hbWU9XCJ1c2VyXCIgcmVxdWlyZWQvPlxyXG4gICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgIHBsYWNlaG9sZGVyPVwicGFzc3dvcmRcIiBuYW1lPVwicGFzc1wiIHJlcXVpcmVkLz5cclxuICAgICAgICAgIDxidXR0b24gdHlwZT1cInN1Ym1pdFwiPkNyZWF0ZTwvYnV0dG9uPlxyXG4gICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgKX1cclxuICAgIDwvUG9wdXA+XHJcbiAgICAgPGgxPntjb29raWUuZ2V0KFwidXNlcm5hbWVcIil9PC9oMT4gXHJcbiAgICAgICA8dGFibGU+XHJcbiAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgIDx0aD5Vc2Vybm1hZTwvdGg+XHJcbiAgICAgICAgICAgICAgICA8dGg+UGFzc3dvcmQ8L3RoPlxyXG4gICAgICAgICAgICAgICAgPHRoPkVkaXQ8L3RoPlxyXG4gICAgICAgICAgICA8L3RyPiBcclxuICAgICAgICAgICAgXHJcbiAgICAgIHtwcm9wcy5kYXRhLm1hcChkYXRhID0+KFxyXG4gICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgICA8dGQ+e2RhdGEudXNlcm5hbWV9PC90ZD5cclxuICAgICAgICAgICAgICAgIDx0ZD57ZGF0YS5wYXNzd29yZH08L3RkPlxyXG4gICAgICAgICAgICAgICAgPHRkPiBcclxuICAgICAgICAgICAgICAgICAgPHRkPlxyXG4gICAgICAgICAgICAgICAgICAgIDxQb3B1cCBcclxuICAgICAgICAgICAgICAgICAgICAgIHRyaWdnZXI9ezxidXR0b24gY2xhc3NOYW1lPVwiYnV0dG9uXCI+IEVkaXQgPC9idXR0b24+fSBtb2RhbD4gXHJcbiAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgIDxmb3JtIG9uU3VibWl0PXsocHJvcHMpID0+IGVkaXRVc2VyKHByb3BzKX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxoMT5FZGl0PC9oMT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgIHBsYWNlaG9sZGVyPXtkYXRhLnVzZXJuYW1lfSBuYW1lPVwidXNlclwiIHJlcXVpcmVkLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgIHBsYWNlaG9sZGVyPXtkYXRhLnBhc3N3b3JkfSBuYW1lPVwicGFzc1wiIHJlcXVpcmVkLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJoaWRkZW5cIiAgdmFsdWU9e2RhdGEuX2lkfSBuYW1lPVwiaWRcIiByZXF1aXJlZC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cInN1Ym1pdFwiPkVkaXQ8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIDwvUG9wdXA+XHJcbiAgICAgICAgICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgIDx0ZD4gXHJcbiAgICAgICAgICAgICAgICAgIDxQb3B1cCBcclxuICAgICAgICAgICAgICAgICAgICAgIHRyaWdnZXI9ezxidXR0b24gY2xhc3NOYW1lPVwiYnV0dG9uXCI+IERlbGV0ZSA8L2J1dHRvbj59PiBcclxuICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgPGZvcm0gb25TdWJtaXQ9eyhwcm9wcykgPT4gZGVsZXRlVXNlcihwcm9wcyl9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxoMT5EZWxldGUgPzwvaDE+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiaGlkZGVuXCIgIHZhbHVlPXtkYXRhLl9pZH0gbmFtZT1cImlkXCIvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIj5EZWxldGU8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgPC9Qb3B1cD5cclxuICAgICAgICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgKSl9XHJcbiAgXHJcbiAgIFxyXG4gICAgICAgIDwvdGFibGU+XHJcbiAgICAgXHJcbiAgICA8L2Rpdj5cclxuICA8L0xheW91dD5cclxuICBcclxuICA8c3R5bGUganN4PntgXHJcblxyXG4gICAgICBoMSB7IHRleHQtYWxpZ246IGNlbnRlcjt9XHJcbiAgICAgIGZvcm0gYnV0dG9uMSB7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgICAgICBmb250LWZhbWlseTogXCJSb2JvdG9cIiwgc2Fucy1zZXJpZjtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIG91dGxpbmU6IDA7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzhCQTFFOTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBib3JkZXI6IDA7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICBjb2xvcjogI0ZGRkZGRjtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4zIGVhc2U7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogYWxsIDAuMyBlYXNlO1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgfVxyXG5cclxuICAgICAgZm9ybSB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcclxuICAgICAgICBwYWRkaW5nOiA0NXB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgfVxyXG5cclxuICAgICAgZm9ybSBoIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMDBweDtcclxuICAgICAgICBib3JkZXI6IDE7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGZvcm0gaW5wdXQge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xyXG4gICAgICAgIG91dGxpbmU6IDA7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2YyZjJmMjtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBib3JkZXI6IDA7XHJcbiAgICAgICAgbWFyZ2luOiAwIDAgMTVweDtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICB9XHJcbiAgICAgIGZvcm0gYnV0dG9uIHtcclxuICAgICAgICBmb250LWZhbWlseTogXCJSb2JvdG9cIiwgc2Fucy1zZXJpZjtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIG91dGxpbmU6IDA7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzRDQUY1MDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgIGNvbG9yOiAjRkZGRkZGO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjMgZWFzZTtcclxuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4zIGVhc2U7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICB9XHJcbiAgICAgIGZvcm0gYnV0dG9uOmhvdmVyLC5mb3JtIGJ1dHRvbjphY3RpdmUsLmZvcm0gYnV0dG9uOmZvY3VzIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjNDNBMDQ3O1xyXG4gICAgICB9XHJcbiAgICAgIGZvcm0gLm1lc3NhZ2Uge1xyXG4gICAgICAgIG1hcmdpbjogMTVweCAwIDA7XHJcbiAgICAgICAgY29sb3I6ICNiM2IzYjM7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICB9XHJcbiAgICAgIGZvcm0gLm1lc3NhZ2UgYSB7XHJcbiAgICAgICAgY29sb3I6ICM0Q0FGNTA7XHJcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICB9XHJcbiAgICAgIGZvcm0gLnJlZ2lzdGVyLWZvcm0ge1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIG1vZGFsIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgXHJcbiAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgfVxyXG4gICAgICBtb2RhbCA+IC5oZWFkZXIge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgXHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgIH1cclxuICAgICAgbW9kYWwgPiAuY29udGVudCB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgcGFkZGluZzogMTBweCA1cHg7XHJcbiAgICAgIH1cclxuICAgICAgbW9kYWwgPiAuYWN0aW9ucyB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgcGFkZGluZzogMTBweCA1cHg7XHJcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgfVxyXG4gICAgXHJcbiAgICAgIHRhYmxlIHtcclxuICAgICAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICB0aCwgdGQge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgfVxyXG4gICAgICBcclxuICAgICAgdHI6bnRoLWNoaWxkKGV2ZW4pe2JhY2tncm91bmQtY29sb3I6ICNmMmYyZjJ9XHJcbiAgICAgIFxyXG4gICAgICB0aCB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzRDQUY1MDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIGxvZ2luLXBhZ2Uge1xyXG4gICAgICAgIHdpZHRoOiAzNjBweDtcclxuICAgICAgICBwYWRkaW5nOiA4JSAwIDA7XHJcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgICB9XHJcbiAgICAgXHJcbiAgICAgIGRpdiB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcclxuICAgICAgICBtYXgtd2lkdGg6IDEwMDBweDtcclxuICAgICAgICBtYXJnaW46IDAgYXV0byAwcHg7XHJcbiAgICAgICBcclxuICAgICAgICBwYWRkaW5nOiA0NXB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBib3gtc2hhZG93OiAwIDAgMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA1cHggNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjI0KTtcclxuICAgICAgfVxyXG4gICAgICBkaXYgaCB7XHJcbiAgICAgICAgbWFyZ2luOiAyMHB4O1xyXG4gICAgICAgIGJvcmRlcjogMTtcclxuICAgICAgfVxyXG4gICAgICBkaXYgaW5wdXQge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xyXG4gICAgICAgIG91dGxpbmU6IDA7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2YyZjJmMjtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBib3JkZXI6IDA7XHJcbiAgICAgICAgbWFyZ2luOiAwIDAgMTVweDtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICB9XHJcbiAgICAgIGRpdiBidXR0b24ge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xyXG4gICAgICAgIHRleHQtdHJhbnNkaXY6IHVwcGVyY2FzZTtcclxuICAgICAgICBvdXRsaW5lOiAwO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICM0Q0FGNTA7XHJcbiAgICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICAgIGZsb2F0IDogcmlnaHQ7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICBjb2xvcjogI0ZGRkZGRjtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4zIGVhc2U7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogYWxsIDAuMyBlYXNlO1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgfVxyXG4gICAgICBkaXYgYnV0dG9uOmhvdmVyLC5kaXYgYnV0dG9uOmFjdGl2ZSwuZGl2IGJ1dHRvbjpmb2N1cyB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzQzQTA0NztcclxuICAgICAgfVxyXG4gICAgICBkaXYgLm1lc3NhZ2Uge1xyXG4gICAgICAgIG1hcmdpbjogMTVweCAwIDA7XHJcbiAgICAgICAgY29sb3I6ICNiM2IzYjM7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICB9XHJcbiAgICAgIGRpdiAubWVzc2FnZSBhIHtcclxuICAgICAgICBjb2xvcjogIzRDQUY1MDtcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgIH1cclxuICAgICAgZGl2IC5yZWdpc3Rlci1kaXYge1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgIH1cclxuICAgICAgY29udGFpbmVyIHtcclxuICAgICAgICBcclxuICAgICAgICB6LWluZGV4OiAxO1xyXG4gICAgICAgIG1heC13aWR0aDogNTAwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgIH1cclxuICAgICAgY29udGFpbmVyOmJlZm9yZSwgLmNvbnRhaW5lcjphZnRlciB7XHJcbiAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIGNsZWFyOiBib3RoO1xyXG4gICAgICB9XHJcbiAgICAgIGNvbnRhaW5lciAuaW5mbyB7XHJcbiAgICAgICAgbWFyZ2luOiA1MHB4IGF1dG87XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB9XHJcbiAgICAgIGNvbnRhaW5lciAuaW5mbyBoMSB7XHJcbiAgICAgICAgbWFyZ2luOiAwIDAgMTVweDtcclxuICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgICAgICBmb250LXdlaWdodDogMzAwO1xyXG4gICAgICAgIGNvbG9yOiAjMWExYTFhO1xyXG4gICAgICB9XHJcbiAgICAgIGNvbnRhaW5lciAuaW5mbyBzcGFuIHtcclxuICAgICAgIGNvbG9yOiAjNGQ0ZDRkO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgfVxyXG4gICAgICBjb250YWluZXIgLmluZm8gc3BhbiBhIHtcclxuICAgICAgICBjb2xvcjogIzAwMDAwMDtcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgIH1cclxuICAgIGNvbnRhaW5lciAuaW5mbyBzcGFuIC5mYSB7XHJcbiAgICAgICAgY29sb3I6ICNFRjNCM0E7XHJcbiAgICAgIH1cclxuICAgICAgYm9keSB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6IFwiUm9ib3RvXCIsIHNhbnMtc2VyaWY7XHJcbiAgICAgICAgLXdlYmtpdC1mb250LXNtb290aGluZzogYW50aWFsaWFzZWQ7XHJcbiAgICAgICAgLW1vei1vc3gtZm9udC1zbW9vdGhpbmc6IGdyYXlzY2FsZTsgICAgICBcclxuICAgICAgfVxyXG4gICAgICBgfTwvc3R5bGU+XHJcbiAgPC9ib2R5PlxyXG4pO1xyXG5cclxuSGVsbG8uZ2V0SW5pdGlhbFByb3BzID0gYXN5bmMgZnVuY3Rpb24oKSB7XHJcbiAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2goJ2h0dHA6Ly9sb2NhbGhvc3Q6Nzc3Ny9hcGkvdjAvL3VzZXInKVxyXG4gIGNvbnN0IGRhdGEgPSBhd2FpdCByZXMuanNvbigpO1xyXG4gIHJldHVybiB7ZGF0YX07XHJcbn1cclxuXHJcblxyXG5cclxuZXhwb3J0IGRlZmF1bHQgSGVsbG8iXX0= */\n/*@ sourceURL=C:\\crud\\pages\\hello.js */"));
};

Hello.getInitialProps =
/*#__PURE__*/
Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
/*#__PURE__*/
_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
  var res, data;
  return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_9___default()('http://localhost:7777/api/v0//user');

        case 2:
          res = _context.sent;
          _context.next = 5;
          return res.json();

        case 5:
          data = _context.sent;
          return _context.abrupt("return", {
            data: data
          });

        case 7:
        case "end":
          return _context.stop();
      }
    }
  }, _callee);
}));
/* harmony default export */ __webpack_exports__["default"] = (Hello);

/***/ })

})
//# sourceMappingURL=hello.js.c7391c79712deea7f5ba.hot-update.js.map