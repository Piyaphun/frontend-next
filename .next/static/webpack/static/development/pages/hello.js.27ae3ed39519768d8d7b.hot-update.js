webpackHotUpdate("static\\development\\pages\\hello.js",{

/***/ "./node_modules/isomorphic-unfetch/browser.js":
false,

/***/ "./node_modules/unfetch/dist/unfetch.mjs":
false,

/***/ "./pages/hello.js":
/*!************************!*\
  !*** ./pages/hello.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime-corejs2/helpers/esm/asyncToGenerator.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/Layout */ "./components/Layout.js");
/* harmony import */ var _components_Navbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/Navbar */ "./components/Navbar.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! js-cookie */ "./node_modules/js-cookie/src/js.cookie.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var reactjs_popup__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! reactjs-popup */ "./node_modules/reactjs-popup/reactjs-popup.es.js");


var _jsxFileName = "C:\\crud\\pages\\hello.js";


var __jsx = react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement;







var editUser = function editUser(props) {
  props.preventDefault(); // หยุดการทำงาน Submi;

  var user = props.target.user.value;
  var pass = props.target.pass.value;
  var id = props.target.id.value;
  axios__WEBPACK_IMPORTED_MODULE_8___default.a.put("http://localhost:7777/api/v0/user", {
    username: user,
    password: pass,
    _id: id
  }).then(function (res) {
    alert("แก้ไขเรียบร้อย");
    next_router__WEBPACK_IMPORTED_MODULE_7___default.a.push({
      pathname: '/hello'
    });
  });
};

var createUser = function createUser(props) {
  props.preventDefault(); // หยุดการทำงาน Submit

  var user = props.target.user.value;
  var pass = props.target.pass.value;
  axios__WEBPACK_IMPORTED_MODULE_8___default.a.post("http://localhost:7777/api/v0/user", {
    username: user,
    password: pass
  }).then(function (res) {
    alert("สมัครเรียบร้อยแล้ว");
    next_router__WEBPACK_IMPORTED_MODULE_7___default.a.push({
      pathname: '/hello'
    });
    props.onClose;
  });
};

var deleteUser = function deleteUser(props) {
  props.preventDefault(); // หยุดการทำงาน Submit

  var id = props.target.id.value;
  props.preventDefault(); // หยุดการทำงาน Submit

  axios__WEBPACK_IMPORTED_MODULE_8___default.a["delete"]("http://localhost:7777/api/v0/user?_id=".concat(id)).then(function (res) {
    console.log(res);

    if (res.data.status == 1) {
      alert("ลบเรียบร้อย");
      next_router__WEBPACK_IMPORTED_MODULE_7___default.a.push({
        pathname: '/hello'
      });
    }
  });
};

var Hello = function Hello(props) {
  return __jsx("body", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72
    },
    __self: this
  }, __jsx(_components_Navbar__WEBPACK_IMPORTED_MODULE_5__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73
    },
    __self: this
  }), __jsx(_components_Layout__WEBPACK_IMPORTED_MODULE_4__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75
    },
    __self: this
  }, __jsx("div", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76
    },
    __self: this
  }, __jsx(reactjs_popup__WEBPACK_IMPORTED_MODULE_9__["default"], {
    trigger: __jsx("button", {
      className: "jsx-3629234234" + " " + "button",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 78
      },
      __self: this
    }, " Create "),
    modal: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77
    },
    __self: this
  }, function (close) {
    return __jsx("form", {
      onSubmit: function onSubmit(props) {
        return createUser(props);
      },
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80
      },
      __self: this
    }, __jsx("h1", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81
      },
      __self: this
    }, "Create"), __jsx("input", {
      type: "text",
      placeholder: "username",
      name: "user",
      required: true,
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82
      },
      __self: this
    }), __jsx("input", {
      type: "text",
      placeholder: "password",
      name: "pass",
      required: true,
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 83
      },
      __self: this
    }), __jsx("button", {
      type: "submit",
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 84
      },
      __self: this
    }, "Create"));
  }), __jsx("h1", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 88
    },
    __self: this
  }, js_cookie__WEBPACK_IMPORTED_MODULE_6___default.a.get("username")), __jsx("table", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 89
    },
    __self: this
  }, __jsx("tr", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 90
    },
    __self: this
  }, __jsx("th", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 91
    },
    __self: this
  }, "Usernmae"), __jsx("th", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 92
    },
    __self: this
  }, "Password"), __jsx("th", {
    className: "jsx-3629234234",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 93
    },
    __self: this
  }, "Edit")), props.data.map(function (data) {
    return __jsx("tr", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 97
      },
      __self: this
    }, __jsx("td", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98
      },
      __self: this
    }, data.username), __jsx("td", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99
      },
      __self: this
    }, data.password), __jsx("td", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 100
      },
      __self: this
    }, __jsx("td", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 101
      },
      __self: this
    }, __jsx(reactjs_popup__WEBPACK_IMPORTED_MODULE_9__["default"], {
      trigger: __jsx("button", {
        className: "jsx-3629234234" + " " + "button",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 103
        },
        __self: this
      }, " Edit "),
      modal: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 102
      },
      __self: this
    }, __jsx("form", {
      onSubmit: function onSubmit(props) {
        return editUser(props);
      },
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 105
      },
      __self: this
    }, __jsx("h1", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 106
      },
      __self: this
    }, "Edit"), __jsx("input", {
      type: "text",
      placeholder: data.username,
      name: "user",
      required: true,
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 107
      },
      __self: this
    }), __jsx("input", {
      type: "text",
      placeholder: data.password,
      name: "pass",
      required: true,
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 108
      },
      __self: this
    }), __jsx("input", {
      type: "hidden",
      value: data._id,
      name: "id",
      required: true,
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 109
      },
      __self: this
    }), __jsx("button", {
      type: "submit",
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 110
      },
      __self: this
    }, "Edit")))), __jsx("td", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 115
      },
      __self: this
    }, __jsx(reactjs_popup__WEBPACK_IMPORTED_MODULE_9__["default"], {
      trigger: __jsx("button", {
        className: "jsx-3629234234" + " " + "button",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        },
        __self: this
      }, " Delete "),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 116
      },
      __self: this
    }, __jsx("form", {
      onSubmit: function onSubmit(props) {
        return deleteUser(props);
      },
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 119
      },
      __self: this
    }, __jsx("h1", {
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 120
      },
      __self: this
    }, "Delete ?"), __jsx("input", {
      type: "hidden",
      value: data._id,
      name: "id",
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 121
      },
      __self: this
    }), __jsx("button", {
      type: "submit",
      className: "jsx-3629234234",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 122
      },
      __self: this
    }, "Delete"))))));
  })))), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "3629234234",
    __self: this
  }, "h1.jsx-3629234234{text-align:center;}form.jsx-3629234234 button1.jsx-3629234234{border-radius:12px;font-family:\"Roboto\",sans-serif;text-transform:uppercase;outline:0;background:#8BA1E9;width:100%;border:0;padding:15px;color:#FFFFFF;font-size:14px;-webkit-transition:all 0.3 ease;-webkit-transition:all 0.3 ease;transition:all 0.3 ease;cursor:pointer;}form.jsx-3629234234{background:#FFFFFF;padding:45px;text-align:center;}form.jsx-3629234234 h.jsx-3629234234{margin-top:100px;border:1;}form.jsx-3629234234 input.jsx-3629234234{font-family:\"Roboto\",sans-serif;outline:0;background:#f2f2f2;width:100%;border:0;margin:0 0 15px;padding:15px;box-sizing:border-box;font-size:14px;}form.jsx-3629234234 button.jsx-3629234234{font-family:\"Roboto\",sans-serif;text-transform:uppercase;outline:0;background:#4CAF50;width:100%;border-radius:12px;border:0;padding:15px;color:#FFFFFF;font-size:14px;-webkit-transition:all 0.3 ease;-webkit-transition:all 0.3 ease;transition:all 0.3 ease;cursor:pointer;}form.jsx-3629234234 button.jsx-3629234234:hover,.form.jsx-3629234234 button.jsx-3629234234:active,.form.jsx-3629234234 button.jsx-3629234234:focus{background:#43A047;}form.jsx-3629234234 .message.jsx-3629234234{margin:15px 0 0;color:#b3b3b3;font-size:12px;}form.jsx-3629234234 .message.jsx-3629234234 a.jsx-3629234234{color:#4CAF50;-webkit-text-decoration:none;text-decoration:none;}form.jsx-3629234234 .register-form.jsx-3629234234{display:none;}modal.jsx-3629234234{position:relative;padding:5px;}modal.jsx-3629234234>.header.jsx-3629234234{width:100%;font-size:18px;text-align:center;padding:5px;}modal.jsx-3629234234>.content.jsx-3629234234{width:100%;padding:10px 5px;}modal.jsx-3629234234>.actions.jsx-3629234234{width:100%;padding:10px 5px;margin:auto;text-align:center;}table.jsx-3629234234{border-collapse:collapse;width:100%;}th.jsx-3629234234,td.jsx-3629234234{text-align:left;padding:10px;}tr.jsx-3629234234:nth-child(even){background-color:#f2f2f2;}th.jsx-3629234234{background-color:#4CAF50;color:white;}login-page.jsx-3629234234{width:360px;padding:8% 0 0;margin:auto;}div.jsx-3629234234{position:relative;z-index:1;background:#FFFFFF;max-width:1000px;margin:0 auto 0px;padding:45px;text-align:center;box-shadow:0 0 20px 0 rgba(0,0,0,0.2),0 5px 5px 0 rgba(0,0,0,0.24);}div.jsx-3629234234 h.jsx-3629234234{margin:20px;border:1;}div.jsx-3629234234 input.jsx-3629234234{font-family:\"Roboto\",sans-serif;outline:0;background:#f2f2f2;width:100%;border:0;margin:0 0 15px;padding:15px;box-sizing:border-box;font-size:14px;}div.jsx-3629234234 button.jsx-3629234234{font-family:\"Roboto\",sans-serif;text-transdiv:uppercase;outline:0;background:#4CAF50;border:0;float :right;padding:15px;color:#FFFFFF;font-size:14px;-webkit-transition:all 0.3 ease;-webkit-transition:all 0.3 ease;transition:all 0.3 ease;cursor:pointer;}div.jsx-3629234234 button.jsx-3629234234:hover,.div.jsx-3629234234 button.jsx-3629234234:active,.div.jsx-3629234234 button.jsx-3629234234:focus{background:#43A047;}div.jsx-3629234234 .message.jsx-3629234234{margin:15px 0 0;color:#b3b3b3;font-size:12px;}div.jsx-3629234234 .message.jsx-3629234234 a.jsx-3629234234{color:#4CAF50;-webkit-text-decoration:none;text-decoration:none;}div.jsx-3629234234 .register-div.jsx-3629234234{display:none;}container.jsx-3629234234{z-index:1;max-width:500px;margin:0 auto;}container.jsx-3629234234:before,.container.jsx-3629234234:after{content:\"\";display:block;clear:both;}container.jsx-3629234234 .info.jsx-3629234234{margin:50px auto;text-align:center;}container.jsx-3629234234 .info.jsx-3629234234 h1.jsx-3629234234{margin:0 0 15px;padding:0;font-size:36px;font-weight:300;color:#1a1a1a;}container.jsx-3629234234 .info.jsx-3629234234 span.jsx-3629234234{color:#4d4d4d;font-size:12px;}container.jsx-3629234234 .info.jsx-3629234234 span.jsx-3629234234 a.jsx-3629234234{color:#000000;-webkit-text-decoration:none;text-decoration:none;}container.jsx-3629234234 .info.jsx-3629234234 span.jsx-3629234234 .fa.jsx-3629234234{color:#EF3B3A;}body.jsx-3629234234{font-family:\"Roboto\",sans-serif;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxjcnVkXFxwYWdlc1xcaGVsbG8uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBd0ljLEFBRzZCLEFBRUQsQUFnQkEsQUFNRixBQUtnQixBQVdBLEFBZWQsQUFHSCxBQUtGLEFBSUQsQUFJSyxBQUtQLEFBT0EsQUFJQSxBQU9jLEFBS1QsQUFJd0IsQUFHZixBQUtiLEFBTU0sQUFXTixBQUlxQixBQVdBLEFBY2QsQUFHSCxBQUtGLEFBSUQsQUFJSCxBQUtBLEFBS08sQUFJRCxBQU9ILEFBSUMsQUFJQSxBQUdtQixVQS9CakIsQ0FoR0QsQUFNRSxBQUlBLEFBMkZILENBbkVDLEFBaUJOLENBN0RYLEFBc0dBLENBMUd1QixBQXNHQSxBQTZCTixBQUlNLEFBSXZCLEVBaEpnQixBQXlDRCxBQTZEQyxBQTJCSixDQW5LRCxBQStKUyxDQXhMRyxBQXlFWCxBQTZDQSxDQW5IdUIsQUFnQnBCLEFBcUNmLEFBc0dBLEVBNUJBLElBbkNhLEFBUThCLEFBSTdCLEFBeUVELENBMUpiLEFBcURvQixBQWdHSixBQWNDLENBNUVILENBNUJkLEFBSWMsQUE4Qk8sQ0FsQnJCLEFBK0ZBLENBeElpQixBQWNqQixBQXdGaUIsRUE5SUcsQUFVUixBQVdlLEFBNEZmLEFBV2MsQUE4RFcsR0FyQnJDLENBekZBLEFBcUZBLENBekVBLEVBTUEsQ0F4Qm9CLEFBc0ZwQixDQWNrQixDQS9KRyxBQXVHQSxFQXREUCxDQW5CZCxBQXNHQSxFQTNDbUIsR0FuR25CLENBakIyQixLQTZFM0IsQUFnRVksQ0F2R0EsQUFxSkksQ0FwR2hCLEdBM0RhLEFBdUdBLEdBdEViLEFBd0RvQixBQThDcEIsQUFpQ0EsRUF0RHFCLENBdkdBLEFBb0tlLElBZnBDLENBL0pXLEFBdUdBLElBbklDLEtBNkJNLEFBdUdBLENBZEgsR0F3QkosQ0E3SVUsQUFzQ1IsUUF3R0UsQ0F4QkssRUF6RkwsQUFVTSxBQTZGTixJQXNFZixJQXpNYSxFQThJRSxHQWpIUyxBQXVHQSxHQWRvRCxHQXJIakUsQUFzQ0EsSUF3R0ssS0E3SUQsQUFzQ0EsT0FWRSxBQXVHQSxFQVdBLElBN0lELEFBc0NBLFNBVmhCLEFBdUdBLEVBV2tDLEdBN0lqQixBQXNDQSxlQXJDaUIsQUFzQ0EsYUE0RWxDLENBMkIwQixrQkE1SUEsQUFzQ0Esc0NBdUdULGVBQ2pCLEdBN0lpQixBQXNDQSxlQXJDakIsQUFzQ0EiLCJmaWxlIjoiQzpcXGNydWRcXHBhZ2VzXFxoZWxsby5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBMYXlvdXQgZnJvbSAnLi4vY29tcG9uZW50cy9MYXlvdXQnXHJcbmltcG9ydCBOYXZiYXIgZnJvbSAnLi4vY29tcG9uZW50cy9OYXZiYXInXHJcbmltcG9ydCBjb29raWUgZnJvbSBcImpzLWNvb2tpZVwiO1xyXG5pbXBvcnQgUm91dGVyIGZyb20gJ25leHQvcm91dGVyJ1xyXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnXHJcbmltcG9ydCBQb3B1cCBmcm9tIFwicmVhY3Rqcy1wb3B1cFwiO1xyXG5cclxuICBjb25zdCBlZGl0VXNlciA9IChwcm9wcykgPT4ge1xyXG4gICAgcHJvcHMucHJldmVudERlZmF1bHQoKSAvLyDguKvguKLguLjguJTguIHguLLguKPguJfguLPguIfguLLguJkgU3VibWk7XHJcbiAgICBjb25zdCB1c2VyID0gcHJvcHMudGFyZ2V0LnVzZXIudmFsdWVcclxuICAgIGNvbnN0IHBhc3MgPSBwcm9wcy50YXJnZXQucGFzcy52YWx1ZVxyXG4gICAgY29uc3QgaWQgPSBwcm9wcy50YXJnZXQuaWQudmFsdWVcclxuICBcclxuICAgIGF4aW9zLnB1dChgaHR0cDovL2xvY2FsaG9zdDo3Nzc3L2FwaS92MC91c2VyYCx7XHJcbiAgICAgICAgdXNlcm5hbWUgOiB1c2VyLFxyXG4gICAgICAgIHBhc3N3b3JkIDogcGFzcyxcclxuICAgICAgICBfaWQgOiBpZFxyXG4gICAgfSlcclxuICAgIC50aGVuKHJlcz0+e1xyXG4gICAgICBhbGVydChcIuC5geC4geC5ieC5hOC4guC5gOC4o+C4teC4ouC4muC4o+C5ieC4reC4olwiKTtcclxuICAgICAgUm91dGVyLnB1c2goe1xyXG4gICAgICAgIHBhdGhuYW1lOiAnL2hlbGxvJ1xyXG4gICAgICB9KVxyXG4gICAgICB9KVxyXG4gICBcclxuICB9XHJcbiAgXHJcbiAgY29uc3QgY3JlYXRlVXNlciA9IChwcm9wcykgPT4ge1xyXG4gICAgcHJvcHMucHJldmVudERlZmF1bHQoKSAvLyDguKvguKLguLjguJTguIHguLLguKPguJfguLPguIfguLLguJkgU3VibWl0XHJcbiAgICBjb25zdCB1c2VyID0gcHJvcHMudGFyZ2V0LnVzZXIudmFsdWVcclxuICAgIGNvbnN0IHBhc3MgPSBwcm9wcy50YXJnZXQucGFzcy52YWx1ZVxyXG4gIFxyXG4gICAgYXhpb3MucG9zdChgaHR0cDovL2xvY2FsaG9zdDo3Nzc3L2FwaS92MC91c2VyYCx7XHJcbiAgICAgICAgdXNlcm5hbWUgOiB1c2VyLFxyXG4gICAgICAgIHBhc3N3b3JkIDogcGFzc1xyXG4gICAgfSlcclxuICAgIC50aGVuKHJlcz0+e1xyXG4gICAgICBhbGVydChcIuC4quC4oeC4seC4hOC4o+C5gOC4o+C4teC4ouC4muC4o+C5ieC4reC4ouC5geC4peC5ieC4p1wiKTtcclxuICAgICAgICBSb3V0ZXIucHVzaCh7XHJcbiAgICAgIHBhdGhuYW1lOiAnL2hlbGxvJ1xyXG4gICAgfSlcclxuICBcclxuICAgIHByb3BzLm9uQ2xvc2U7XHJcbiAgICB9KVxyXG4gICBcclxuICB9XHJcblxyXG4gIGNvbnN0IGRlbGV0ZVVzZXIgPSAocHJvcHMpID0+IHtcclxuICAgIHByb3BzLnByZXZlbnREZWZhdWx0KCkgLy8g4Lir4Lii4Li44LiU4LiB4Liy4Lij4LiX4Liz4LiH4Liy4LiZIFN1Ym1pdFxyXG4gICAgY29uc3QgaWQ9ICBwcm9wcy50YXJnZXQuaWQudmFsdWVcclxuICAgIHByb3BzLnByZXZlbnREZWZhdWx0KCkgLy8g4Lir4Lii4Li44LiU4LiB4Liy4Lij4LiX4Liz4LiH4Liy4LiZIFN1Ym1pdFxyXG4gXHJcbiAgICBheGlvcy5kZWxldGUoYGh0dHA6Ly9sb2NhbGhvc3Q6Nzc3Ny9hcGkvdjAvdXNlcj9faWQ9JHtpZH1gXHJcbiAgICApXHJcbiAgICAudGhlbihyZXM9PntcclxuICAgICAgY29uc29sZS5sb2cocmVzKTtcclxuICAgICAgaWYocmVzLmRhdGEuc3RhdHVzPT0xKXtcclxuICAgICAgYWxlcnQoXCLguKXguJrguYDguKPguLXguKLguJrguKPguYnguK3guKJcIik7XHJcbiAgICAgIFJvdXRlci5wdXNoKHtcclxuICAgICAgICBwYXRobmFtZTogJy9oZWxsbydcclxuICAgICAgfSlcclxuICAgICAgXHJcbiAgICB9XHJcbiAgICB9KVxyXG4gICBcclxuICB9XHJcblxyXG5cclxuIFxyXG5jb25zdCBIZWxsbyA9IHByb3BzID0+IChcclxuICBcclxuICA8Ym9keT5cclxuICAgPE5hdmJhciAvPlxyXG4gICBcclxuICA8TGF5b3V0PiBcclxuICAgIDxkaXY+XHJcbiAgICA8UG9wdXAgXHJcbiAgICAgICAgdHJpZ2dlcj17PGJ1dHRvbiBjbGFzc05hbWU9XCJidXR0b25cIj4gQ3JlYXRlIDwvYnV0dG9uPn0gbW9kYWwgPiBcclxuICAgICAgICAge2Nsb3NlID0+IChcclxuICAgICAgICA8Zm9ybSBvblN1Ym1pdD17KHByb3BzKSA9PiBjcmVhdGVVc2VyKHByb3BzKX0+XHJcbiAgICAgICAgICA8aDE+Q3JlYXRlPC9oMT5cclxuICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiICBwbGFjZWhvbGRlcj1cInVzZXJuYW1lXCIgbmFtZT1cInVzZXJcIiByZXF1aXJlZC8+XHJcbiAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiAgcGxhY2Vob2xkZXI9XCJwYXNzd29yZFwiIG5hbWU9XCJwYXNzXCIgcmVxdWlyZWQvPlxyXG4gICAgICAgICAgPGJ1dHRvbiB0eXBlPVwic3VibWl0XCI+Q3JlYXRlPC9idXR0b24+XHJcbiAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgICApfVxyXG4gICAgPC9Qb3B1cD5cclxuICAgICA8aDE+e2Nvb2tpZS5nZXQoXCJ1c2VybmFtZVwiKX08L2gxPiBcclxuICAgICAgIDx0YWJsZT5cclxuICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgPHRoPlVzZXJubWFlPC90aD5cclxuICAgICAgICAgICAgICAgIDx0aD5QYXNzd29yZDwvdGg+XHJcbiAgICAgICAgICAgICAgICA8dGg+RWRpdDwvdGg+XHJcbiAgICAgICAgICAgIDwvdHI+IFxyXG4gICAgICAgICAgICBcclxuICAgICAge3Byb3BzLmRhdGEubWFwKGRhdGEgPT4oXHJcbiAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgIDx0ZD57ZGF0YS51c2VybmFtZX08L3RkPlxyXG4gICAgICAgICAgICAgICAgPHRkPntkYXRhLnBhc3N3b3JkfTwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8dGQ+IFxyXG4gICAgICAgICAgICAgICAgICA8dGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPFBvcHVwIFxyXG4gICAgICAgICAgICAgICAgICAgICAgdHJpZ2dlcj17PGJ1dHRvbiBjbGFzc05hbWU9XCJidXR0b25cIj4gRWRpdCA8L2J1dHRvbj59IG1vZGFsPiBcclxuICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgPGZvcm0gb25TdWJtaXQ9eyhwcm9wcykgPT4gZWRpdFVzZXIocHJvcHMpfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGgxPkVkaXQ8L2gxPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiAgcGxhY2Vob2xkZXI9e2RhdGEudXNlcm5hbWV9IG5hbWU9XCJ1c2VyXCIgcmVxdWlyZWQvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiAgcGxhY2Vob2xkZXI9e2RhdGEucGFzc3dvcmR9IG5hbWU9XCJwYXNzXCIgcmVxdWlyZWQvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImhpZGRlblwiICB2YWx1ZT17ZGF0YS5faWR9IG5hbWU9XCJpZFwiIHJlcXVpcmVkLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwic3VibWl0XCI+RWRpdDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgPC9Qb3B1cD5cclxuICAgICAgICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgICAgICAgPHRkPiBcclxuICAgICAgICAgICAgICAgICAgPFBvcHVwIFxyXG4gICAgICAgICAgICAgICAgICAgICAgdHJpZ2dlcj17PGJ1dHRvbiBjbGFzc05hbWU9XCJidXR0b25cIj4gRGVsZXRlIDwvYnV0dG9uPn0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICA8Zm9ybSBvblN1Ym1pdD17KHByb3BzKSA9PiBkZWxldGVVc2VyKHByb3BzKX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGgxPkRlbGV0ZSA/PC9oMT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJoaWRkZW5cIiAgdmFsdWU9e2RhdGEuX2lkfSBuYW1lPVwiaWRcIi8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cInN1Ym1pdFwiPkRlbGV0ZTwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICA8L1BvcHVwPlxyXG4gICAgICAgICAgICAgICAgICA8L3RkPlxyXG4gICAgICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgPC90cj5cclxuICAgICAgICApKX1cclxuICBcclxuICAgXHJcbiAgICAgICAgPC90YWJsZT5cclxuICAgICBcclxuICAgIDwvZGl2PlxyXG4gIDwvTGF5b3V0PlxyXG4gIFxyXG4gIDxzdHlsZSBqc3g+e2BcclxuXHJcbiAgICAgIGgxIHsgdGV4dC1hbGlnbjogY2VudGVyO31cclxuICAgICAgZm9ybSBidXR0b24xIHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgb3V0bGluZTogMDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjOEJBMUU5O1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgIGNvbG9yOiAjRkZGRkZGO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjMgZWFzZTtcclxuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4zIGVhc2U7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBmb3JtIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xyXG4gICAgICAgIHBhZGRpbmc6IDQ1cHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBmb3JtIGgge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDEwMHB4O1xyXG4gICAgICAgIGJvcmRlcjogMTtcclxuICAgICAgfVxyXG5cclxuICAgICAgZm9ybSBpbnB1dCB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6IFwiUm9ib3RvXCIsIHNhbnMtc2VyaWY7XHJcbiAgICAgICAgb3V0bGluZTogMDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjZjJmMmYyO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgICBtYXJnaW46IDAgMCAxNXB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgIH1cclxuICAgICAgZm9ybSBidXR0b24ge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgb3V0bGluZTogMDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjNENBRjUwO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgY29sb3I6ICNGRkZGRkY7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuMyBlYXNlO1xyXG4gICAgICAgIHRyYW5zaXRpb246IGFsbCAwLjMgZWFzZTtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgIH1cclxuICAgICAgZm9ybSBidXR0b246aG92ZXIsLmZvcm0gYnV0dG9uOmFjdGl2ZSwuZm9ybSBidXR0b246Zm9jdXMge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICM0M0EwNDc7XHJcbiAgICAgIH1cclxuICAgICAgZm9ybSAubWVzc2FnZSB7XHJcbiAgICAgICAgbWFyZ2luOiAxNXB4IDAgMDtcclxuICAgICAgICBjb2xvcjogI2IzYjNiMztcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgIH1cclxuICAgICAgZm9ybSAubWVzc2FnZSBhIHtcclxuICAgICAgICBjb2xvcjogIzRDQUY1MDtcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgIH1cclxuICAgICAgZm9ybSAucmVnaXN0ZXItZm9ybSB7XHJcbiAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgbW9kYWwge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICBcclxuICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICB9XHJcbiAgICAgIG1vZGFsID4gLmhlYWRlciB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICBcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgfVxyXG4gICAgICBtb2RhbCA+IC5jb250ZW50IHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBwYWRkaW5nOiAxMHB4IDVweDtcclxuICAgICAgfVxyXG4gICAgICBtb2RhbCA+IC5hY3Rpb25zIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBwYWRkaW5nOiAxMHB4IDVweDtcclxuICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB9XHJcbiAgICBcclxuICAgICAgdGFibGUge1xyXG4gICAgICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIHRoLCB0ZCB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICB0cjpudGgtY2hpbGQoZXZlbil7YmFja2dyb3VuZC1jb2xvcjogI2YyZjJmMn1cclxuICAgICAgXHJcbiAgICAgIHRoIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNENBRjUwO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgfVxyXG4gICAgICBcclxuICAgICAgbG9naW4tcGFnZSB7XHJcbiAgICAgICAgd2lkdGg6IDM2MHB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDglIDAgMDtcclxuICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICAgIH1cclxuICAgICBcclxuICAgICAgZGl2IHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgei1pbmRleDogMTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xyXG4gICAgICAgIG1heC13aWR0aDogMTAwMHB4O1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvIDBweDtcclxuICAgICAgIFxyXG4gICAgICAgIHBhZGRpbmc6IDQ1cHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDAgMCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDVweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMjQpO1xyXG4gICAgICB9XHJcbiAgICAgIGRpdiBoIHtcclxuICAgICAgICBtYXJnaW46IDIwcHg7XHJcbiAgICAgICAgYm9yZGVyOiAxO1xyXG4gICAgICB9XHJcbiAgICAgIGRpdiBpbnB1dCB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6IFwiUm9ib3RvXCIsIHNhbnMtc2VyaWY7XHJcbiAgICAgICAgb3V0bGluZTogMDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjZjJmMmYyO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgICBtYXJnaW46IDAgMCAxNXB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgIH1cclxuICAgICAgZGl2IGJ1dHRvbiB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6IFwiUm9ib3RvXCIsIHNhbnMtc2VyaWY7XHJcbiAgICAgICAgdGV4dC10cmFuc2RpdjogdXBwZXJjYXNlO1xyXG4gICAgICAgIG91dGxpbmU6IDA7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzRDQUY1MDtcclxuICAgICAgICBib3JkZXI6IDA7XHJcbiAgICAgICAgZmxvYXQgOiByaWdodDtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgIGNvbG9yOiAjRkZGRkZGO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjMgZWFzZTtcclxuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4zIGVhc2U7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICB9XHJcbiAgICAgIGRpdiBidXR0b246aG92ZXIsLmRpdiBidXR0b246YWN0aXZlLC5kaXYgYnV0dG9uOmZvY3VzIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjNDNBMDQ3O1xyXG4gICAgICB9XHJcbiAgICAgIGRpdiAubWVzc2FnZSB7XHJcbiAgICAgICAgbWFyZ2luOiAxNXB4IDAgMDtcclxuICAgICAgICBjb2xvcjogI2IzYjNiMztcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgIH1cclxuICAgICAgZGl2IC5tZXNzYWdlIGEge1xyXG4gICAgICAgIGNvbG9yOiAjNENBRjUwO1xyXG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgfVxyXG4gICAgICBkaXYgLnJlZ2lzdGVyLWRpdiB7XHJcbiAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgICAgfVxyXG4gICAgICBjb250YWluZXIge1xyXG4gICAgICAgIFxyXG4gICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgbWF4LXdpZHRoOiA1MDBweDtcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgfVxyXG4gICAgICBjb250YWluZXI6YmVmb3JlLCAuY29udGFpbmVyOmFmdGVyIHtcclxuICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgY2xlYXI6IGJvdGg7XHJcbiAgICAgIH1cclxuICAgICAgY29udGFpbmVyIC5pbmZvIHtcclxuICAgICAgICBtYXJnaW46IDUwcHggYXV0bztcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIH1cclxuICAgICAgY29udGFpbmVyIC5pbmZvIGgxIHtcclxuICAgICAgICBtYXJnaW46IDAgMCAxNXB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiAzMDA7XHJcbiAgICAgICAgY29sb3I6ICMxYTFhMWE7XHJcbiAgICAgIH1cclxuICAgICAgY29udGFpbmVyIC5pbmZvIHNwYW4ge1xyXG4gICAgICAgY29sb3I6ICM0ZDRkNGQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICB9XHJcbiAgICAgIGNvbnRhaW5lciAuaW5mbyBzcGFuIGEge1xyXG4gICAgICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgfVxyXG4gICAgY29udGFpbmVyIC5pbmZvIHNwYW4gLmZhIHtcclxuICAgICAgICBjb2xvcjogI0VGM0IzQTtcclxuICAgICAgfVxyXG4gICAgICBib2R5IHtcclxuICAgICAgICBmb250LWZhbWlseTogXCJSb2JvdG9cIiwgc2Fucy1zZXJpZjtcclxuICAgICAgICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcclxuICAgICAgICAtbW96LW9zeC1mb250LXNtb290aGluZzogZ3JheXNjYWxlOyAgICAgIFxyXG4gICAgICB9XHJcbiAgICAgIGB9PC9zdHlsZT5cclxuICA8L2JvZHk+XHJcbik7XHJcblxyXG5IZWxsby5nZXRJbml0aWFsUHJvcHMgPSBhc3luYyBmdW5jdGlvbigpIHtcclxuICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCgnaHR0cDovL2xvY2FsaG9zdDo3Nzc3L2FwaS92MC8vdXNlcicpXHJcbiAgY29uc3QgZGF0YSA9IGF3YWl0IHJlcy5qc29uKCk7XHJcbiAgcmV0dXJuIHtkYXRhfTtcclxufVxyXG5cclxuXHJcblxyXG5leHBvcnQgZGVmYXVsdCBIZWxsbyJdfQ== */\n/*@ sourceURL=C:\\crud\\pages\\hello.js */"));
};

Hello.getInitialProps =
/*#__PURE__*/
Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
/*#__PURE__*/
_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
  var res, data;
  return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return fetch('http://localhost:7777/api/v0//user');

        case 2:
          res = _context.sent;
          _context.next = 5;
          return res.json();

        case 5:
          data = _context.sent;
          return _context.abrupt("return", {
            data: data
          });

        case 7:
        case "end":
          return _context.stop();
      }
    }
  }, _callee);
}));
/* harmony default export */ __webpack_exports__["default"] = (Hello);

/***/ })

})
//# sourceMappingURL=hello.js.27ae3ed39519768d8d7b.hot-update.js.map