module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/Footer.js":
/*!******************************!*\
  !*** ./components/Footer.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "C:\\crud\\components\\Footer.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
const style = {
  position: 'absolute',
  right: 0,
  bottom: 0,
  left: 0,
  padding: '1rem',
  backgroundColor: '#EEEEEE',
  textAlign: 'center'
};

const Footer = () => __jsx("div", {
  style: style,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 13
  },
  __self: undefined
}, "This is Footer.");

/* harmony default export */ __webpack_exports__["default"] = (Footer);

/***/ }),

/***/ "./components/Header.js":
/*!******************************!*\
  !*** ./components/Header.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "C:\\crud\\components\\Header.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

const style = {
  backgroundColor: '#4CAF50',
  border: 'none',
  color: 'white',
  padding: '10px 10px',
  textAlign: 'center',
  textDecoration: 'none',
  display: 'inline-block',
  fontSize: '16px',
  margin: '4px 2px',
  cursor: 'pointer'
};

const Header = () => __jsx("div", {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 18
  },
  __self: undefined
});

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./components/Layout.js":
/*!******************************!*\
  !*** ./components/Layout.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Header */ "./components/Header.js");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Footer */ "./components/Footer.js");
var _jsxFileName = "C:\\crud\\components\\Layout.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const style = {
  background: '#76b852'
};

const Layout = props => __jsx("body", {
  style: style,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 9
  },
  __self: undefined
}, __jsx("div", {
  style: style,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 10
  },
  __self: undefined
}, __jsx(_Header__WEBPACK_IMPORTED_MODULE_1__["default"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 11
  },
  __self: undefined
}), props.children));

/* harmony default export */ __webpack_exports__["default"] = (Layout);

/***/ }),

/***/ "./components/Navbar.js":
/*!******************************!*\
  !*** ./components/Navbar.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! js-cookie */ "js-cookie");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "C:\\crud\\components\\Navbar.js";


var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;



const Logout = () => {
  js_cookie__WEBPACK_IMPORTED_MODULE_3___default.a.remove("username");
  js_cookie__WEBPACK_IMPORTED_MODULE_3___default.a.remove("token");
  next_router__WEBPACK_IMPORTED_MODULE_2___default.a.push({
    pathname: '/index'
  });
};

const Navbar = () => __jsx("body", {
  className: "jsx-1402021287",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 12
  },
  __self: undefined
}, __jsx("ul", {
  className: "jsx-1402021287",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 13
  },
  __self: undefined
}, __jsx("div", {
  className: "jsx-1402021287",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 14
  },
  __self: undefined
}, __jsx("button", {
  onClick: props => Logout(),
  type: "button",
  className: "jsx-1402021287",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 14
  },
  __self: undefined
}, "logout"))), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
  id: "1402021287",
  __self: undefined
}, "ul.jsx-1402021287{list-style-type:none;margin:0;padding:0;overflow:hidden;background-color:#333;width:100%;}li.jsx-1402021287{float:left;}li.jsx-1402021287 a.jsx-1402021287{display:block;color:white;text-align:center;padding:14px 16px;-webkit-text-decoration:none;text-decoration:none;}li.jsx-1402021287 a.jsx-1402021287:hover{background-color:#111;}div.jsx-1402021287{float :right;}div.jsx-1402021287 button.jsx-1402021287{font-family:\"Roboto\",sans-serif;text-transdiv:uppercase;outline:0;background:#4CAF50;border:0;border-radius:12px;padding:15px;margin-left:10px;margin-top:10px;margin-right:10px;margin-bottom:10px;color:#FFFFFF;font-size:14px;-webkit-transition:all 0.3 ease;-webkit-transition:all 0.3 ease;transition:all 0.3 ease;cursor:pointer;re;}li.jsx-1402021287 button.jsx-1402021287:hover,.div.jsx-1402021287 button.jsx-1402021287:active,.div.jsx-1402021287 button.jsx-1402021287:focus{background:#43A047;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxjcnVkXFxjb21wb25lbnRzXFxOYXZiYXIuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBZ0JjLEFBRzBCLEFBU1YsQUFJRyxBQVFRLEFBR1AsQUFHa0IsQUFxQmQsV0F0Q3JCLEVBZUEsQ0FYYyxLQW1DZCxFQWhEVyxDQXFCWCxJQVBvQixJQWJSLEVBMEJjLFFBekJSLElBYUUsWUFaSSxBQXlCWixNQVpXLElBYUYsWUF6QlQsT0EyQkQsSUExQlgsS0E0QnFCLGtCQWhCckIsQ0FpQmUsYUFDRyxpQkFDRCxnQkFDRSxrQkFDQyxtQkFDSixjQUNDLGVBQ2lCLGdDQUNSLHdEQUNULGVBRWxCLEdBQUMiLCJmaWxlIjoiQzpcXGNydWRcXGNvbXBvbmVudHNcXE5hdmJhci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSb3V0ZXIgZnJvbSAnbmV4dC9yb3V0ZXInXHJcbmltcG9ydCBjb29raWUgZnJvbSBcImpzLWNvb2tpZVwiO1xyXG5cclxuY29uc3QgTG9nb3V0ID0gKCkgPT4ge1xyXG4gICAgY29va2llLnJlbW92ZShcInVzZXJuYW1lXCIpO1xyXG4gICAgY29va2llLnJlbW92ZShcInRva2VuXCIpO1xyXG4gICAgUm91dGVyLnB1c2goe1xyXG4gICAgICBwYXRobmFtZTogJy9pbmRleCdcclxuICAgIH0pXHJcbiAgfVxyXG5jb25zdCBOYXZiYXIgPSAoKSA9PiAoXHJcbiAgICA8Ym9keT5cclxuICA8dWw+XHJcbiAgPGRpdj48YnV0dG9uICBvbkNsaWNrPXsocHJvcHMpID0+TG9nb3V0KCl9IHR5cGU9XCJidXR0b25cIj5sb2dvdXQ8L2J1dHRvbj48L2Rpdj5cclxuPC91bD5cclxuICBcclxuICA8c3R5bGUganN4PntgXHJcbiAgdWwge1xyXG4gICAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzMzO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICB9XHJcbiAgXHJcbiAgbGkge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgfVxyXG4gIFxyXG4gIGxpIGEge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nOiAxNHB4IDE2cHg7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgfVxyXG4gIFxyXG4gIGxpIGE6aG92ZXIge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzExMTtcclxuICB9XHJcbiAgZGl2e1xyXG4gICAgICBmbG9hdCA6IHJpZ2h0O1xyXG4gIH1cclxuICBkaXYgYnV0dG9uIHtcclxuICAgIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xyXG4gICAgdGV4dC10cmFuc2RpdjogdXBwZXJjYXNlO1xyXG4gICAgb3V0bGluZTogMDtcclxuICAgIGJhY2tncm91bmQ6ICM0Q0FGNTA7XHJcblxyXG4gICAgYm9yZGVyOiAwO1xyXG4gICBcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6MTBweDtcclxuICAgIG1hcmdpbi10b3A6MTBweDtcclxuICAgIG1hcmdpbi1yaWdodDoxMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbToxMHB4O1xyXG4gICAgY29sb3I6ICNGRkZGRkY7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjMgZWFzZTtcclxuICAgIHRyYW5zaXRpb246IGFsbCAwLjMgZWFzZTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHJlXHJcbiAgfVxyXG4gIGxpIGJ1dHRvbjpob3ZlciwuZGl2IGJ1dHRvbjphY3RpdmUsLmRpdiBidXR0b246Zm9jdXMge1xyXG4gICAgYmFja2dyb3VuZDogIzQzQTA0NztcclxuICB9XHJcbiAgYH08L3N0eWxlPlxyXG4gIDwvYm9keT5cclxuKVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgTmF2YmFyXHJcbiJdfQ== */\n/*@ sourceURL=C:\\crud\\components\\Navbar.js */"));

/* harmony default export */ __webpack_exports__["default"] = (Navbar);

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/map.js":
/*!************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/map.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/map */ "core-js/library/fn/map");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/assign.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/assign */ "core-js/library/fn/object/assign");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/define-property */ "core-js/library/fn/object/define-property");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js":
/*!*******************************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/get-own-property-descriptor */ "core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/extends.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/extends.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Object$assign = __webpack_require__(/*! ../core-js/object/assign */ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js");

function _extends() {
  module.exports = _extends = _Object$assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

module.exports = _extends;

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js":
/*!******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Object$getOwnPropertyDescriptor = __webpack_require__(/*! ../core-js/object/get-own-property-descriptor */ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js");

var _Object$defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  } else {
    var newObj = {};

    if (obj != null) {
      for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
          var desc = _Object$defineProperty && _Object$getOwnPropertyDescriptor ? _Object$getOwnPropertyDescriptor(obj, key) : {};

          if (desc.get || desc.set) {
            _Object$defineProperty(newObj, key, desc);
          } else {
            newObj[key] = obj[key];
          }
        }
      }
    }

    newObj["default"] = obj;
    return newObj;
  }
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ "./node_modules/next/dist/client/link.js":
/*!***********************************************!*\
  !*** ./node_modules/next/dist/client/link.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireWildcard */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js");

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.default = void 0;

var _map = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/core-js/map */ "./node_modules/@babel/runtime-corejs2/core-js/map.js"));

var _url = __webpack_require__(/*! url */ "url");

var _react = _interopRequireWildcard(__webpack_require__(/*! react */ "react"));

var _propTypes = _interopRequireDefault(__webpack_require__(/*! prop-types */ "prop-types"));

var _router = _interopRequireDefault(__webpack_require__(/*! ./router */ "./node_modules/next/dist/client/router.js"));

var _rewriteUrlForExport = __webpack_require__(/*! next-server/dist/lib/router/rewrite-url-for-export */ "next-server/dist/lib/router/rewrite-url-for-export");

var _utils = __webpack_require__(/*! next-server/dist/lib/utils */ "next-server/dist/lib/utils");
/* global __NEXT_DATA__ */


function isLocal(href) {
  const url = (0, _url.parse)(href, false, true);
  const origin = (0, _url.parse)((0, _utils.getLocationOrigin)(), false, true);
  return !url.host || url.protocol === origin.protocol && url.host === origin.host;
}

function memoizedFormatUrl(formatFunc) {
  let lastHref = null;
  let lastAs = null;
  let lastResult = null;
  return (href, as) => {
    if (lastResult && href === lastHref && as === lastAs) {
      return lastResult;
    }

    const result = formatFunc(href, as);
    lastHref = href;
    lastAs = as;
    lastResult = result;
    return result;
  };
}

function formatUrl(url) {
  return url && typeof url === 'object' ? (0, _utils.formatWithValidation)(url) : url;
}

let observer;
const listeners = new _map.default();
const IntersectionObserver = false ? undefined : null;

function getObserver() {
  // Return shared instance of IntersectionObserver if already created
  if (observer) {
    return observer;
  } // Only create shared IntersectionObserver if supported in browser


  if (!IntersectionObserver) {
    return undefined;
  }

  return observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (!listeners.has(entry.target)) {
        return;
      }

      const cb = listeners.get(entry.target);

      if (entry.isIntersecting || entry.intersectionRatio > 0) {
        observer.unobserve(entry.target);
        listeners.delete(entry.target);
        cb();
      }
    });
  }, {
    rootMargin: '200px'
  });
}

const listenToIntersections = (el, cb) => {
  const observer = getObserver();

  if (!observer) {
    return () => {};
  }

  observer.observe(el);
  listeners.set(el, cb);
  return () => {
    observer.unobserve(el);
    listeners.delete(el);
  };
};

class Link extends _react.Component {
  constructor(props) {
    super(props);
    this.p = void 0;

    this.cleanUpListeners = () => {};

    this.formatUrls = memoizedFormatUrl((href, asHref) => {
      return {
        href: formatUrl(href),
        as: asHref ? formatUrl(asHref) : asHref
      };
    });

    this.linkClicked = e => {
      // @ts-ignore target exists on currentTarget
      const {
        nodeName,
        target
      } = e.currentTarget;

      if (nodeName === 'A' && (target && target !== '_self' || e.metaKey || e.ctrlKey || e.shiftKey || e.nativeEvent && e.nativeEvent.which === 2)) {
        // ignore click for new tab / new window behavior
        return;
      }

      let {
        href,
        as
      } = this.formatUrls(this.props.href, this.props.as);

      if (!isLocal(href)) {
        // ignore click if it's outside our scope (e.g. https://google.com)
        return;
      }

      const {
        pathname
      } = window.location;
      href = (0, _url.resolve)(pathname, href);
      as = as ? (0, _url.resolve)(pathname, as) : href;
      e.preventDefault(); //  avoid scroll for urls with anchor refs

      let {
        scroll
      } = this.props;

      if (scroll == null) {
        scroll = as.indexOf('#') < 0;
      } // replace state instead of push if prop is present


      _router.default[this.props.replace ? 'replace' : 'push'](href, as, {
        shallow: this.props.shallow
      }).then(success => {
        if (!success) return;

        if (scroll) {
          window.scrollTo(0, 0);
          document.body.focus();
        }
      });
    };

    if (true) {
      if (props.prefetch) {
        console.warn('Next.js auto-prefetches automatically based on viewport. The prefetch attribute is no longer needed. More: https://err.sh/zeit/next.js/prefetch-true-deprecated');
      }
    }

    this.p = props.prefetch !== false;
  }

  componentWillUnmount() {
    this.cleanUpListeners();
  }

  handleRef(ref) {
    if (this.p && IntersectionObserver && ref && ref.tagName) {
      this.cleanUpListeners();
      this.cleanUpListeners = listenToIntersections(ref, () => {
        this.prefetch();
      });
    }
  } // The function is memoized so that no extra lifecycles are needed
  // as per https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html


  prefetch() {
    if (!this.p || true) return; // Prefetch the JSON page if asked (only in the client)

    const {
      pathname
    } = window.location;
    const {
      href: parsedHref
    } = this.formatUrls(this.props.href, this.props.as);
    const href = (0, _url.resolve)(pathname, parsedHref);

    _router.default.prefetch(href);
  }

  render() {
    let {
      children
    } = this.props;
    const {
      href,
      as
    } = this.formatUrls(this.props.href, this.props.as); // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

    if (typeof children === 'string') {
      children = _react.default.createElement("a", null, children);
    } // This will return the first child, if multiple are provided it will throw an error


    const child = _react.Children.only(children);

    const props = {
      ref: el => {
        this.handleRef(el);

        if (child && typeof child === 'object' && child.ref) {
          if (typeof child.ref === 'function') child.ref(el);else if (typeof child.ref === 'object') {
            child.ref.current = el;
          }
        }
      },
      onMouseEnter: e => {
        if (child.props && typeof child.props.onMouseEnter === 'function') {
          child.props.onMouseEnter(e);
        }

        this.prefetch();
      },
      onClick: e => {
        if (child.props && typeof child.props.onClick === 'function') {
          child.props.onClick(e);
        }

        if (!e.defaultPrevented) {
          this.linkClicked(e);
        }
      } // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
      // defined, we specify the current 'href', so that repetition is not needed by the user

    };

    if (this.props.passHref || child.type === 'a' && !('href' in child.props)) {
      props.href = as || href;
    } // Add the ending slash to the paths. So, we can serve the
    // "<page>/index.html" directly.


    if (false) {}

    return _react.default.cloneElement(child, props);
  }

}

Link.propTypes = void 0;

if (true) {
  const warn = (0, _utils.execOnce)(console.error); // This module gets removed by webpack.IgnorePlugin

  const exact = __webpack_require__(/*! prop-types-exact */ "prop-types-exact");

  Link.propTypes = exact({
    href: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]).isRequired,
    as: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]),
    prefetch: _propTypes.default.bool,
    replace: _propTypes.default.bool,
    shallow: _propTypes.default.bool,
    passHref: _propTypes.default.bool,
    scroll: _propTypes.default.bool,
    children: _propTypes.default.oneOfType([_propTypes.default.element, (props, propName) => {
      const value = props[propName];

      if (typeof value === 'string') {
        warn("Warning: You're using a string directly inside <Link>. This usage has been deprecated. Please add an <a> tag as child of <Link>");
      }

      return null;
    }]).isRequired
  });
}

var _default = Link;
exports.default = _default;

/***/ }),

/***/ "./node_modules/next/dist/client/router.js":
/*!*************************************************!*\
  !*** ./node_modules/next/dist/client/router.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireWildcard */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js");

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _extends2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/helpers/extends */ "./node_modules/@babel/runtime-corejs2/helpers/extends.js"));

var _defineProperty = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js"));

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _router2 = _interopRequireWildcard(__webpack_require__(/*! next-server/dist/lib/router/router */ "next-server/dist/lib/router/router"));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__(/*! next-server/dist/lib/router-context */ "next-server/dist/lib/router-context");

var _withRouter = _interopRequireDefault(__webpack_require__(/*! ./with-router */ "./node_modules/next/dist/client/with-router.js"));

exports.withRouter = _withRouter.default;
/* global window */

const singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

const urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components'];
const routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
const coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

(0, _defineProperty.default)(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  (0, _defineProperty.default)(singletonRouter, field, {
    get() {
      const router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = function () {
    const router = getRouter();
    return router[field](...arguments);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, function () {
      const eventField = "on" + event.charAt(0).toUpperCase() + event.substring(1);
      const _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...arguments);
        } catch (err) {
          // tslint:disable-next-line:no-console
          console.error("Error when running the Router event: " + eventField); // tslint:disable-next-line:no-console

          console.error(err.message + "\n" + err.stack);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    const message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


const createRouter = function createRouter() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  const _router = router;
  const instance = {};

  for (const property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = (0, _extends2.default)({}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = function () {
      return _router[field](...arguments);
    };
  });
  return instance;
}

/***/ }),

/***/ "./node_modules/next/dist/client/with-router.js":
/*!******************************************************!*\
  !*** ./node_modules/next/dist/client/with-router.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.default = withRouter;

var _extends2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/helpers/extends */ "./node_modules/@babel/runtime-corejs2/helpers/extends.js"));

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _propTypes = _interopRequireDefault(__webpack_require__(/*! prop-types */ "prop-types"));

function withRouter(ComposedComponent) {
  class WithRouteWrapper extends _react.default.Component {
    constructor() {
      super(...arguments);
      this.context = void 0;
    }

    render() {
      return _react.default.createElement(ComposedComponent, (0, _extends2.default)({
        router: this.context.router
      }, this.props));
    }

  }

  WithRouteWrapper.displayName = void 0;
  WithRouteWrapper.getInitialProps = void 0;
  WithRouteWrapper.contextTypes = {
    router: _propTypes.default.object
  };
  WithRouteWrapper.getInitialProps = ComposedComponent.getInitialProps;

  if (true) {
    const name = ComposedComponent.displayName || ComposedComponent.name || 'Unknown';
    WithRouteWrapper.displayName = "withRouter(" + name + ")";
  }

  return WithRouteWrapper;
}

/***/ }),

/***/ "./node_modules/next/link.js":
/*!***********************************!*\
  !*** ./node_modules/next/link.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/client/link */ "./node_modules/next/dist/client/link.js")


/***/ }),

/***/ "./pages/hello.js":
/*!************************!*\
  !*** ./pages/hello.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Layout */ "./components/Layout.js");
/* harmony import */ var _components_Navbar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Navbar */ "./components/Navbar.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! js-cookie */ "js-cookie");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! isomorphic-unfetch */ "isomorphic-unfetch");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var reactjs_popup__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! reactjs-popup */ "reactjs-popup");
/* harmony import */ var reactjs_popup__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(reactjs_popup__WEBPACK_IMPORTED_MODULE_8__);
var _jsxFileName = "C:\\crud\\pages\\hello.js";


var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;








const editUser = props => {
  props.preventDefault(); // หยุดการทำงาน Submi;

  const user = props.target.user.value;
  const pass = props.target.pass.value;
  const id = props.target.id.value;
  axios__WEBPACK_IMPORTED_MODULE_6___default.a.put(`http://localhost:7777/api/v0/user`, {
    username: user,
    password: pass,
    _id: id
  }).then(res => {
    alert("แก้ไขเรียบร้อย");
    next_router__WEBPACK_IMPORTED_MODULE_5___default.a.push({
      pathname: '/hello'
    });
  });
};

const createUser = props => {
  props.preventDefault(); // หยุดการทำงาน Submit

  const user = props.target.user.value;
  const pass = props.target.pass.value;
  axios__WEBPACK_IMPORTED_MODULE_6___default.a.post(`http://localhost:7777/api/v0/user`, {
    username: user,
    password: pass
  }).then(res => {
    alert("สมัครเรียบร้อยแล้ว");
    next_router__WEBPACK_IMPORTED_MODULE_5___default.a.push({
      pathname: '/hello'
    });
    props.onClose;
  });
};

const deleteUser = props => {
  props.preventDefault(); // หยุดการทำงาน Submit

  const id = props.target.id.value;
  props.preventDefault(); // หยุดการทำงาน Submit

  axios__WEBPACK_IMPORTED_MODULE_6___default.a.delete(`http://localhost:7777/api/v0/user?_id=${id}`).then(res => {
    console.log(res);

    if (res.data.status == 1) {
      alert("ลบเรียบร้อย");
      next_router__WEBPACK_IMPORTED_MODULE_5___default.a.push({
        pathname: '/hello'
      });
    }
  });
};

const Hello = props => __jsx("body", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 73
  },
  __self: undefined
}, __jsx(_components_Navbar__WEBPACK_IMPORTED_MODULE_3__["default"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 74
  },
  __self: undefined
}), __jsx(_components_Layout__WEBPACK_IMPORTED_MODULE_2__["default"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 76
  },
  __self: undefined
}, __jsx("div", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 77
  },
  __self: undefined
}, __jsx(reactjs_popup__WEBPACK_IMPORTED_MODULE_8___default.a, {
  trigger: __jsx("button", {
    className: "jsx-3629234234" + " " + "button",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79
    },
    __self: undefined
  }, " Create "),
  modal: true,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 78
  },
  __self: undefined
}, close => __jsx("form", {
  onSubmit: props => createUser(props),
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 81
  },
  __self: undefined
}, __jsx("h1", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 82
  },
  __self: undefined
}, "Create"), __jsx("input", {
  type: "text",
  placeholder: "username",
  name: "user",
  required: true,
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 83
  },
  __self: undefined
}), __jsx("input", {
  type: "text",
  placeholder: "password",
  name: "pass",
  required: true,
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 84
  },
  __self: undefined
}), __jsx("button", {
  type: "submit",
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 85
  },
  __self: undefined
}, "Create"))), __jsx("h1", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 89
  },
  __self: undefined
}, js_cookie__WEBPACK_IMPORTED_MODULE_4___default.a.get("username")), __jsx("table", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 90
  },
  __self: undefined
}, __jsx("tr", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 91
  },
  __self: undefined
}, __jsx("th", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 92
  },
  __self: undefined
}, "Usernmae"), __jsx("th", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 93
  },
  __self: undefined
}, "Password"), __jsx("th", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 94
  },
  __self: undefined
}, "Edit")), props.data.map(data => __jsx("tr", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 98
  },
  __self: undefined
}, __jsx("td", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 99
  },
  __self: undefined
}, data.username), __jsx("td", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 100
  },
  __self: undefined
}, data.password), __jsx("td", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 101
  },
  __self: undefined
}, __jsx("td", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 102
  },
  __self: undefined
}, __jsx(reactjs_popup__WEBPACK_IMPORTED_MODULE_8___default.a, {
  trigger: __jsx("button", {
    className: "jsx-3629234234" + " " + "button",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 104
    },
    __self: undefined
  }, " Edit "),
  modal: true,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 103
  },
  __self: undefined
}, __jsx("form", {
  onSubmit: props => editUser(props),
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 106
  },
  __self: undefined
}, __jsx("h1", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 107
  },
  __self: undefined
}, "Edit"), __jsx("input", {
  type: "text",
  placeholder: data.username,
  name: "user",
  required: true,
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 108
  },
  __self: undefined
}), __jsx("input", {
  type: "text",
  placeholder: data.password,
  name: "pass",
  required: true,
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 109
  },
  __self: undefined
}), __jsx("input", {
  type: "hidden",
  value: data._id,
  name: "id",
  required: true,
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 110
  },
  __self: undefined
}), __jsx("button", {
  type: "submit",
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 111
  },
  __self: undefined
}, "Edit")))), __jsx("td", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 116
  },
  __self: undefined
}, __jsx(reactjs_popup__WEBPACK_IMPORTED_MODULE_8___default.a, {
  trigger: __jsx("button", {
    className: "jsx-3629234234" + " " + "button",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 118
    },
    __self: undefined
  }, " Delete "),
  __source: {
    fileName: _jsxFileName,
    lineNumber: 117
  },
  __self: undefined
}, __jsx("form", {
  onSubmit: props => deleteUser(props),
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 120
  },
  __self: undefined
}, __jsx("h1", {
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 121
  },
  __self: undefined
}, "Delete ?"), __jsx("input", {
  type: "hidden",
  value: data._id,
  name: "id",
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 122
  },
  __self: undefined
}), __jsx("button", {
  type: "submit",
  className: "jsx-3629234234",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 123
  },
  __self: undefined
}, "Delete")))))))))), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
  id: "3629234234",
  __self: undefined
}, "h1.jsx-3629234234{text-align:center;}form.jsx-3629234234 button1.jsx-3629234234{border-radius:12px;font-family:\"Roboto\",sans-serif;text-transform:uppercase;outline:0;background:#8BA1E9;width:100%;border:0;padding:15px;color:#FFFFFF;font-size:14px;-webkit-transition:all 0.3 ease;-webkit-transition:all 0.3 ease;transition:all 0.3 ease;cursor:pointer;}form.jsx-3629234234{background:#FFFFFF;padding:45px;text-align:center;}form.jsx-3629234234 h.jsx-3629234234{margin-top:100px;border:1;}form.jsx-3629234234 input.jsx-3629234234{font-family:\"Roboto\",sans-serif;outline:0;background:#f2f2f2;width:100%;border:0;margin:0 0 15px;padding:15px;box-sizing:border-box;font-size:14px;}form.jsx-3629234234 button.jsx-3629234234{font-family:\"Roboto\",sans-serif;text-transform:uppercase;outline:0;background:#4CAF50;width:100%;border-radius:12px;border:0;padding:15px;color:#FFFFFF;font-size:14px;-webkit-transition:all 0.3 ease;-webkit-transition:all 0.3 ease;transition:all 0.3 ease;cursor:pointer;}form.jsx-3629234234 button.jsx-3629234234:hover,.form.jsx-3629234234 button.jsx-3629234234:active,.form.jsx-3629234234 button.jsx-3629234234:focus{background:#43A047;}form.jsx-3629234234 .message.jsx-3629234234{margin:15px 0 0;color:#b3b3b3;font-size:12px;}form.jsx-3629234234 .message.jsx-3629234234 a.jsx-3629234234{color:#4CAF50;-webkit-text-decoration:none;text-decoration:none;}form.jsx-3629234234 .register-form.jsx-3629234234{display:none;}modal.jsx-3629234234{position:relative;padding:5px;}modal.jsx-3629234234>.header.jsx-3629234234{width:100%;font-size:18px;text-align:center;padding:5px;}modal.jsx-3629234234>.content.jsx-3629234234{width:100%;padding:10px 5px;}modal.jsx-3629234234>.actions.jsx-3629234234{width:100%;padding:10px 5px;margin:auto;text-align:center;}table.jsx-3629234234{border-collapse:collapse;width:100%;}th.jsx-3629234234,td.jsx-3629234234{text-align:left;padding:10px;}tr.jsx-3629234234:nth-child(even){background-color:#f2f2f2;}th.jsx-3629234234{background-color:#4CAF50;color:white;}login-page.jsx-3629234234{width:360px;padding:8% 0 0;margin:auto;}div.jsx-3629234234{position:relative;z-index:1;background:#FFFFFF;max-width:1000px;margin:0 auto 0px;padding:45px;text-align:center;box-shadow:0 0 20px 0 rgba(0,0,0,0.2),0 5px 5px 0 rgba(0,0,0,0.24);}div.jsx-3629234234 h.jsx-3629234234{margin:20px;border:1;}div.jsx-3629234234 input.jsx-3629234234{font-family:\"Roboto\",sans-serif;outline:0;background:#f2f2f2;width:100%;border:0;margin:0 0 15px;padding:15px;box-sizing:border-box;font-size:14px;}div.jsx-3629234234 button.jsx-3629234234{font-family:\"Roboto\",sans-serif;text-transdiv:uppercase;outline:0;background:#4CAF50;border:0;float :right;padding:15px;color:#FFFFFF;font-size:14px;-webkit-transition:all 0.3 ease;-webkit-transition:all 0.3 ease;transition:all 0.3 ease;cursor:pointer;}div.jsx-3629234234 button.jsx-3629234234:hover,.div.jsx-3629234234 button.jsx-3629234234:active,.div.jsx-3629234234 button.jsx-3629234234:focus{background:#43A047;}div.jsx-3629234234 .message.jsx-3629234234{margin:15px 0 0;color:#b3b3b3;font-size:12px;}div.jsx-3629234234 .message.jsx-3629234234 a.jsx-3629234234{color:#4CAF50;-webkit-text-decoration:none;text-decoration:none;}div.jsx-3629234234 .register-div.jsx-3629234234{display:none;}container.jsx-3629234234{z-index:1;max-width:500px;margin:0 auto;}container.jsx-3629234234:before,.container.jsx-3629234234:after{content:\"\";display:block;clear:both;}container.jsx-3629234234 .info.jsx-3629234234{margin:50px auto;text-align:center;}container.jsx-3629234234 .info.jsx-3629234234 h1.jsx-3629234234{margin:0 0 15px;padding:0;font-size:36px;font-weight:300;color:#1a1a1a;}container.jsx-3629234234 .info.jsx-3629234234 span.jsx-3629234234{color:#4d4d4d;font-size:12px;}container.jsx-3629234234 .info.jsx-3629234234 span.jsx-3629234234 a.jsx-3629234234{color:#000000;-webkit-text-decoration:none;text-decoration:none;}container.jsx-3629234234 .info.jsx-3629234234 span.jsx-3629234234 .fa.jsx-3629234234{color:#EF3B3A;}body.jsx-3629234234{font-family:\"Roboto\",sans-serif;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxjcnVkXFxwYWdlc1xcaGVsbG8uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBeUljLEFBRzZCLEFBRUQsQUFnQkEsQUFNRixBQUtnQixBQVdBLEFBZWQsQUFHSCxBQUtGLEFBSUQsQUFJSyxBQUtQLEFBT0EsQUFJQSxBQU9jLEFBS1QsQUFJd0IsQUFHZixBQUtiLEFBTU0sQUFXTixBQUlxQixBQVdBLEFBY2QsQUFHSCxBQUtGLEFBSUQsQUFJSCxBQUtBLEFBS08sQUFJRCxBQU9ILEFBSUMsQUFJQSxBQUdtQixVQS9CakIsQ0FoR0QsQUFNRSxBQUlBLEFBMkZILENBbkVDLEFBaUJOLENBN0RYLEFBc0dBLENBMUd1QixBQXNHQSxBQTZCTixBQUlNLEFBSXZCLEVBaEpnQixBQXlDRCxBQTZEQyxBQTJCSixDQW5LRCxBQStKUyxDQXhMRyxBQXlFWCxBQTZDQSxDQW5IdUIsQUFnQnBCLEFBcUNmLEFBc0dBLEVBNUJBLElBbkNhLEFBUThCLEFBSTdCLEFBeUVELENBMUpiLEFBcURvQixBQWdHSixBQWNDLENBNUVILENBNUJkLEFBSWMsQUE4Qk8sQ0FsQnJCLEFBK0ZBLENBeElpQixBQWNqQixBQXdGaUIsRUE5SUcsQUFVUixBQVdlLEFBNEZmLEFBV2MsQUE4RFcsR0FyQnJDLENBekZBLEFBcUZBLENBekVBLEVBTUEsQ0F4Qm9CLEFBc0ZwQixDQWNrQixDQS9KRyxBQXVHQSxFQXREUCxDQW5CZCxBQXNHQSxFQTNDbUIsR0FuR25CLENBakIyQixLQTZFM0IsQUFnRVksQ0F2R0EsQUFxSkksQ0FwR2hCLEdBM0RhLEFBdUdBLEdBdEViLEFBd0RvQixBQThDcEIsQUFpQ0EsRUF0RHFCLENBdkdBLEFBb0tlLElBZnBDLENBL0pXLEFBdUdBLElBbklDLEtBNkJNLEFBdUdBLENBZEgsR0F3QkosQ0E3SVUsQUFzQ1IsUUF3R0UsQ0F4QkssRUF6RkwsQUFVTSxBQTZGTixJQXNFZixJQXpNYSxFQThJRSxHQWpIUyxBQXVHQSxHQWRvRCxHQXJIakUsQUFzQ0EsSUF3R0ssS0E3SUQsQUFzQ0EsT0FWRSxBQXVHQSxFQVdBLElBN0lELEFBc0NBLFNBVmhCLEFBdUdBLEVBV2tDLEdBN0lqQixBQXNDQSxlQXJDaUIsQUFzQ0EsYUE0RWxDLENBMkIwQixrQkE1SUEsQUFzQ0Esc0NBdUdULGVBQ2pCLEdBN0lpQixBQXNDQSxlQXJDakIsQUFzQ0EiLCJmaWxlIjoiQzpcXGNydWRcXHBhZ2VzXFxoZWxsby5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBMYXlvdXQgZnJvbSAnLi4vY29tcG9uZW50cy9MYXlvdXQnXHJcbmltcG9ydCBOYXZiYXIgZnJvbSAnLi4vY29tcG9uZW50cy9OYXZiYXInXHJcbmltcG9ydCBjb29raWUgZnJvbSBcImpzLWNvb2tpZVwiO1xyXG5pbXBvcnQgUm91dGVyIGZyb20gJ25leHQvcm91dGVyJ1xyXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnXHJcbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLXVuZmV0Y2gnO1xyXG5pbXBvcnQgUG9wdXAgZnJvbSBcInJlYWN0anMtcG9wdXBcIjtcclxuXHJcbiAgY29uc3QgZWRpdFVzZXIgPSAocHJvcHMpID0+IHtcclxuICAgIHByb3BzLnByZXZlbnREZWZhdWx0KCkgLy8g4Lir4Lii4Li44LiU4LiB4Liy4Lij4LiX4Liz4LiH4Liy4LiZIFN1Ym1pO1xyXG4gICAgY29uc3QgdXNlciA9IHByb3BzLnRhcmdldC51c2VyLnZhbHVlXHJcbiAgICBjb25zdCBwYXNzID0gcHJvcHMudGFyZ2V0LnBhc3MudmFsdWVcclxuICAgIGNvbnN0IGlkID0gcHJvcHMudGFyZ2V0LmlkLnZhbHVlXHJcbiAgXHJcbiAgICBheGlvcy5wdXQoYGh0dHA6Ly9sb2NhbGhvc3Q6Nzc3Ny9hcGkvdjAvdXNlcmAse1xyXG4gICAgICAgIHVzZXJuYW1lIDogdXNlcixcclxuICAgICAgICBwYXNzd29yZCA6IHBhc3MsXHJcbiAgICAgICAgX2lkIDogaWRcclxuICAgIH0pXHJcbiAgICAudGhlbihyZXM9PntcclxuICAgICAgYWxlcnQoXCLguYHguIHguYnguYTguILguYDguKPguLXguKLguJrguKPguYnguK3guKJcIik7XHJcbiAgICAgIFJvdXRlci5wdXNoKHtcclxuICAgICAgICBwYXRobmFtZTogJy9oZWxsbydcclxuICAgICAgfSlcclxuICAgICAgfSlcclxuICAgXHJcbiAgfVxyXG4gIFxyXG4gIGNvbnN0IGNyZWF0ZVVzZXIgPSAocHJvcHMpID0+IHtcclxuICAgIHByb3BzLnByZXZlbnREZWZhdWx0KCkgLy8g4Lir4Lii4Li44LiU4LiB4Liy4Lij4LiX4Liz4LiH4Liy4LiZIFN1Ym1pdFxyXG4gICAgY29uc3QgdXNlciA9IHByb3BzLnRhcmdldC51c2VyLnZhbHVlXHJcbiAgICBjb25zdCBwYXNzID0gcHJvcHMudGFyZ2V0LnBhc3MudmFsdWVcclxuICBcclxuICAgIGF4aW9zLnBvc3QoYGh0dHA6Ly9sb2NhbGhvc3Q6Nzc3Ny9hcGkvdjAvdXNlcmAse1xyXG4gICAgICAgIHVzZXJuYW1lIDogdXNlcixcclxuICAgICAgICBwYXNzd29yZCA6IHBhc3NcclxuICAgIH0pXHJcbiAgICAudGhlbihyZXM9PntcclxuICAgICAgYWxlcnQoXCLguKrguKHguLHguITguKPguYDguKPguLXguKLguJrguKPguYnguK3guKLguYHguKXguYnguKdcIik7XHJcbiAgICAgICAgUm91dGVyLnB1c2goe1xyXG4gICAgICBwYXRobmFtZTogJy9oZWxsbydcclxuICAgIH0pXHJcbiAgXHJcbiAgICBwcm9wcy5vbkNsb3NlO1xyXG4gICAgfSlcclxuICAgXHJcbiAgfVxyXG5cclxuICBjb25zdCBkZWxldGVVc2VyID0gKHByb3BzKSA9PiB7XHJcbiAgICBwcm9wcy5wcmV2ZW50RGVmYXVsdCgpIC8vIOC4q+C4ouC4uOC4lOC4geC4suC4o+C4l+C4s+C4h+C4suC4mSBTdWJtaXRcclxuICAgIGNvbnN0IGlkPSAgcHJvcHMudGFyZ2V0LmlkLnZhbHVlXHJcbiAgICBwcm9wcy5wcmV2ZW50RGVmYXVsdCgpIC8vIOC4q+C4ouC4uOC4lOC4geC4suC4o+C4l+C4s+C4h+C4suC4mSBTdWJtaXRcclxuIFxyXG4gICAgYXhpb3MuZGVsZXRlKGBodHRwOi8vbG9jYWxob3N0Ojc3NzcvYXBpL3YwL3VzZXI/X2lkPSR7aWR9YFxyXG4gICAgKVxyXG4gICAgLnRoZW4ocmVzPT57XHJcbiAgICAgIGNvbnNvbGUubG9nKHJlcyk7XHJcbiAgICAgIGlmKHJlcy5kYXRhLnN0YXR1cz09MSl7XHJcbiAgICAgIGFsZXJ0KFwi4Lil4Lia4LmA4Lij4Li14Lii4Lia4Lij4LmJ4Lit4LiiXCIpO1xyXG4gICAgICBSb3V0ZXIucHVzaCh7XHJcbiAgICAgICAgcGF0aG5hbWU6ICcvaGVsbG8nXHJcbiAgICAgIH0pXHJcbiAgICAgIFxyXG4gICAgfVxyXG4gICAgfSlcclxuICAgXHJcbiAgfVxyXG5cclxuXHJcbiBcclxuY29uc3QgSGVsbG8gPSBwcm9wcyA9PiAoXHJcbiAgXHJcbiAgPGJvZHk+XHJcbiAgIDxOYXZiYXIgLz5cclxuICAgXHJcbiAgPExheW91dD4gXHJcbiAgICA8ZGl2PlxyXG4gICAgPFBvcHVwIFxyXG4gICAgICAgIHRyaWdnZXI9ezxidXR0b24gY2xhc3NOYW1lPVwiYnV0dG9uXCI+IENyZWF0ZSA8L2J1dHRvbj59IG1vZGFsID4gXHJcbiAgICAgICAgIHtjbG9zZSA9PiAoXHJcbiAgICAgICAgPGZvcm0gb25TdWJtaXQ9eyhwcm9wcykgPT4gY3JlYXRlVXNlcihwcm9wcyl9PlxyXG4gICAgICAgICAgPGgxPkNyZWF0ZTwvaDE+XHJcbiAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiAgcGxhY2Vob2xkZXI9XCJ1c2VybmFtZVwiIG5hbWU9XCJ1c2VyXCIgcmVxdWlyZWQvPlxyXG4gICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgIHBsYWNlaG9sZGVyPVwicGFzc3dvcmRcIiBuYW1lPVwicGFzc1wiIHJlcXVpcmVkLz5cclxuICAgICAgICAgIDxidXR0b24gdHlwZT1cInN1Ym1pdFwiPkNyZWF0ZTwvYnV0dG9uPlxyXG4gICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgKX1cclxuICAgIDwvUG9wdXA+XHJcbiAgICAgPGgxPntjb29raWUuZ2V0KFwidXNlcm5hbWVcIil9PC9oMT4gXHJcbiAgICAgICA8dGFibGU+XHJcbiAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgIDx0aD5Vc2Vybm1hZTwvdGg+XHJcbiAgICAgICAgICAgICAgICA8dGg+UGFzc3dvcmQ8L3RoPlxyXG4gICAgICAgICAgICAgICAgPHRoPkVkaXQ8L3RoPlxyXG4gICAgICAgICAgICA8L3RyPiBcclxuICAgICAgICAgICAgXHJcbiAgICAgIHtwcm9wcy5kYXRhLm1hcChkYXRhID0+KFxyXG4gICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgICA8dGQ+e2RhdGEudXNlcm5hbWV9PC90ZD5cclxuICAgICAgICAgICAgICAgIDx0ZD57ZGF0YS5wYXNzd29yZH08L3RkPlxyXG4gICAgICAgICAgICAgICAgPHRkPiBcclxuICAgICAgICAgICAgICAgICAgPHRkPlxyXG4gICAgICAgICAgICAgICAgICAgIDxQb3B1cCBcclxuICAgICAgICAgICAgICAgICAgICAgIHRyaWdnZXI9ezxidXR0b24gY2xhc3NOYW1lPVwiYnV0dG9uXCI+IEVkaXQgPC9idXR0b24+fSBtb2RhbD4gXHJcbiAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgIDxmb3JtIG9uU3VibWl0PXsocHJvcHMpID0+IGVkaXRVc2VyKHByb3BzKX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxoMT5FZGl0PC9oMT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgIHBsYWNlaG9sZGVyPXtkYXRhLnVzZXJuYW1lfSBuYW1lPVwidXNlclwiIHJlcXVpcmVkLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgIHBsYWNlaG9sZGVyPXtkYXRhLnBhc3N3b3JkfSBuYW1lPVwicGFzc1wiIHJlcXVpcmVkLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJoaWRkZW5cIiAgdmFsdWU9e2RhdGEuX2lkfSBuYW1lPVwiaWRcIiByZXF1aXJlZC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cInN1Ym1pdFwiPkVkaXQ8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIDwvUG9wdXA+XHJcbiAgICAgICAgICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgIDx0ZD4gXHJcbiAgICAgICAgICAgICAgICAgIDxQb3B1cCBcclxuICAgICAgICAgICAgICAgICAgICAgIHRyaWdnZXI9ezxidXR0b24gY2xhc3NOYW1lPVwiYnV0dG9uXCI+IERlbGV0ZSA8L2J1dHRvbj59PiBcclxuICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgPGZvcm0gb25TdWJtaXQ9eyhwcm9wcykgPT4gZGVsZXRlVXNlcihwcm9wcyl9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxoMT5EZWxldGUgPzwvaDE+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiaGlkZGVuXCIgIHZhbHVlPXtkYXRhLl9pZH0gbmFtZT1cImlkXCIvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIj5EZWxldGU8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgPC9Qb3B1cD5cclxuICAgICAgICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgKSl9XHJcbiAgXHJcbiAgIFxyXG4gICAgICAgIDwvdGFibGU+XHJcbiAgICAgXHJcbiAgICA8L2Rpdj5cclxuICA8L0xheW91dD5cclxuICBcclxuICA8c3R5bGUganN4PntgXHJcblxyXG4gICAgICBoMSB7IHRleHQtYWxpZ246IGNlbnRlcjt9XHJcbiAgICAgIGZvcm0gYnV0dG9uMSB7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgICAgICBmb250LWZhbWlseTogXCJSb2JvdG9cIiwgc2Fucy1zZXJpZjtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIG91dGxpbmU6IDA7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzhCQTFFOTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBib3JkZXI6IDA7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICBjb2xvcjogI0ZGRkZGRjtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4zIGVhc2U7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogYWxsIDAuMyBlYXNlO1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgfVxyXG5cclxuICAgICAgZm9ybSB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcclxuICAgICAgICBwYWRkaW5nOiA0NXB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgfVxyXG5cclxuICAgICAgZm9ybSBoIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMDBweDtcclxuICAgICAgICBib3JkZXI6IDE7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGZvcm0gaW5wdXQge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xyXG4gICAgICAgIG91dGxpbmU6IDA7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2YyZjJmMjtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBib3JkZXI6IDA7XHJcbiAgICAgICAgbWFyZ2luOiAwIDAgMTVweDtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICB9XHJcbiAgICAgIGZvcm0gYnV0dG9uIHtcclxuICAgICAgICBmb250LWZhbWlseTogXCJSb2JvdG9cIiwgc2Fucy1zZXJpZjtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIG91dGxpbmU6IDA7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzRDQUY1MDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgIGNvbG9yOiAjRkZGRkZGO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjMgZWFzZTtcclxuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4zIGVhc2U7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICB9XHJcbiAgICAgIGZvcm0gYnV0dG9uOmhvdmVyLC5mb3JtIGJ1dHRvbjphY3RpdmUsLmZvcm0gYnV0dG9uOmZvY3VzIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjNDNBMDQ3O1xyXG4gICAgICB9XHJcbiAgICAgIGZvcm0gLm1lc3NhZ2Uge1xyXG4gICAgICAgIG1hcmdpbjogMTVweCAwIDA7XHJcbiAgICAgICAgY29sb3I6ICNiM2IzYjM7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICB9XHJcbiAgICAgIGZvcm0gLm1lc3NhZ2UgYSB7XHJcbiAgICAgICAgY29sb3I6ICM0Q0FGNTA7XHJcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICB9XHJcbiAgICAgIGZvcm0gLnJlZ2lzdGVyLWZvcm0ge1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIG1vZGFsIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgXHJcbiAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgfVxyXG4gICAgICBtb2RhbCA+IC5oZWFkZXIge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgXHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgIH1cclxuICAgICAgbW9kYWwgPiAuY29udGVudCB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgcGFkZGluZzogMTBweCA1cHg7XHJcbiAgICAgIH1cclxuICAgICAgbW9kYWwgPiAuYWN0aW9ucyB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgcGFkZGluZzogMTBweCA1cHg7XHJcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgfVxyXG4gICAgXHJcbiAgICAgIHRhYmxlIHtcclxuICAgICAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICB0aCwgdGQge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgfVxyXG4gICAgICBcclxuICAgICAgdHI6bnRoLWNoaWxkKGV2ZW4pe2JhY2tncm91bmQtY29sb3I6ICNmMmYyZjJ9XHJcbiAgICAgIFxyXG4gICAgICB0aCB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzRDQUY1MDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIGxvZ2luLXBhZ2Uge1xyXG4gICAgICAgIHdpZHRoOiAzNjBweDtcclxuICAgICAgICBwYWRkaW5nOiA4JSAwIDA7XHJcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgICB9XHJcbiAgICAgXHJcbiAgICAgIGRpdiB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcclxuICAgICAgICBtYXgtd2lkdGg6IDEwMDBweDtcclxuICAgICAgICBtYXJnaW46IDAgYXV0byAwcHg7XHJcbiAgICAgICBcclxuICAgICAgICBwYWRkaW5nOiA0NXB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBib3gtc2hhZG93OiAwIDAgMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA1cHggNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjI0KTtcclxuICAgICAgfVxyXG4gICAgICBkaXYgaCB7XHJcbiAgICAgICAgbWFyZ2luOiAyMHB4O1xyXG4gICAgICAgIGJvcmRlcjogMTtcclxuICAgICAgfVxyXG4gICAgICBkaXYgaW5wdXQge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xyXG4gICAgICAgIG91dGxpbmU6IDA7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2YyZjJmMjtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBib3JkZXI6IDA7XHJcbiAgICAgICAgbWFyZ2luOiAwIDAgMTVweDtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICB9XHJcbiAgICAgIGRpdiBidXR0b24ge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xyXG4gICAgICAgIHRleHQtdHJhbnNkaXY6IHVwcGVyY2FzZTtcclxuICAgICAgICBvdXRsaW5lOiAwO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICM0Q0FGNTA7XHJcbiAgICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICAgIGZsb2F0IDogcmlnaHQ7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICBjb2xvcjogI0ZGRkZGRjtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4zIGVhc2U7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogYWxsIDAuMyBlYXNlO1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgfVxyXG4gICAgICBkaXYgYnV0dG9uOmhvdmVyLC5kaXYgYnV0dG9uOmFjdGl2ZSwuZGl2IGJ1dHRvbjpmb2N1cyB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzQzQTA0NztcclxuICAgICAgfVxyXG4gICAgICBkaXYgLm1lc3NhZ2Uge1xyXG4gICAgICAgIG1hcmdpbjogMTVweCAwIDA7XHJcbiAgICAgICAgY29sb3I6ICNiM2IzYjM7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICB9XHJcbiAgICAgIGRpdiAubWVzc2FnZSBhIHtcclxuICAgICAgICBjb2xvcjogIzRDQUY1MDtcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgIH1cclxuICAgICAgZGl2IC5yZWdpc3Rlci1kaXYge1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgIH1cclxuICAgICAgY29udGFpbmVyIHtcclxuICAgICAgICBcclxuICAgICAgICB6LWluZGV4OiAxO1xyXG4gICAgICAgIG1heC13aWR0aDogNTAwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgIH1cclxuICAgICAgY29udGFpbmVyOmJlZm9yZSwgLmNvbnRhaW5lcjphZnRlciB7XHJcbiAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIGNsZWFyOiBib3RoO1xyXG4gICAgICB9XHJcbiAgICAgIGNvbnRhaW5lciAuaW5mbyB7XHJcbiAgICAgICAgbWFyZ2luOiA1MHB4IGF1dG87XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB9XHJcbiAgICAgIGNvbnRhaW5lciAuaW5mbyBoMSB7XHJcbiAgICAgICAgbWFyZ2luOiAwIDAgMTVweDtcclxuICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgICAgICBmb250LXdlaWdodDogMzAwO1xyXG4gICAgICAgIGNvbG9yOiAjMWExYTFhO1xyXG4gICAgICB9XHJcbiAgICAgIGNvbnRhaW5lciAuaW5mbyBzcGFuIHtcclxuICAgICAgIGNvbG9yOiAjNGQ0ZDRkO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgfVxyXG4gICAgICBjb250YWluZXIgLmluZm8gc3BhbiBhIHtcclxuICAgICAgICBjb2xvcjogIzAwMDAwMDtcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgIH1cclxuICAgIGNvbnRhaW5lciAuaW5mbyBzcGFuIC5mYSB7XHJcbiAgICAgICAgY29sb3I6ICNFRjNCM0E7XHJcbiAgICAgIH1cclxuICAgICAgYm9keSB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6IFwiUm9ib3RvXCIsIHNhbnMtc2VyaWY7XHJcbiAgICAgICAgLXdlYmtpdC1mb250LXNtb290aGluZzogYW50aWFsaWFzZWQ7XHJcbiAgICAgICAgLW1vei1vc3gtZm9udC1zbW9vdGhpbmc6IGdyYXlzY2FsZTsgICAgICBcclxuICAgICAgfVxyXG4gICAgICBgfTwvc3R5bGU+XHJcbiAgPC9ib2R5PlxyXG4pO1xyXG5cclxuSGVsbG8uZ2V0SW5pdGlhbFByb3BzID0gYXN5bmMgZnVuY3Rpb24oKSB7XHJcbiAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2goJ2h0dHA6Ly9sb2NhbGhvc3Q6Nzc3Ny9hcGkvdjAvL3VzZXInKVxyXG4gIGNvbnN0IGRhdGEgPSBhd2FpdCByZXMuanNvbigpO1xyXG4gIHJldHVybiB7ZGF0YX07XHJcbn1cclxuXHJcblxyXG5cclxuZXhwb3J0IGRlZmF1bHQgSGVsbG8iXX0= */\n/*@ sourceURL=C:\\crud\\pages\\hello.js */"));

Hello.getInitialProps = async function () {
  const res = await isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_7___default()('http://localhost:7777/api/v0//user');
  const data = await res.json();
  return {
    data
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Hello);

/***/ }),

/***/ 3:
/*!******************************!*\
  !*** multi ./pages/hello.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\crud\pages\hello.js */"./pages/hello.js");


/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "core-js/library/fn/map":
/*!*****************************************!*\
  !*** external "core-js/library/fn/map" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/map");

/***/ }),

/***/ "core-js/library/fn/object/assign":
/*!***************************************************!*\
  !*** external "core-js/library/fn/object/assign" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/assign");

/***/ }),

/***/ "core-js/library/fn/object/define-property":
/*!************************************************************!*\
  !*** external "core-js/library/fn/object/define-property" ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-property");

/***/ }),

/***/ "core-js/library/fn/object/get-own-property-descriptor":
/*!************************************************************************!*\
  !*** external "core-js/library/fn/object/get-own-property-descriptor" ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "isomorphic-unfetch":
/*!*************************************!*\
  !*** external "isomorphic-unfetch" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ "js-cookie":
/*!****************************!*\
  !*** external "js-cookie" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("js-cookie");

/***/ }),

/***/ "next-server/dist/lib/router-context":
/*!******************************************************!*\
  !*** external "next-server/dist/lib/router-context" ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-server/dist/lib/router-context");

/***/ }),

/***/ "next-server/dist/lib/router/rewrite-url-for-export":
/*!*********************************************************************!*\
  !*** external "next-server/dist/lib/router/rewrite-url-for-export" ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-server/dist/lib/router/rewrite-url-for-export");

/***/ }),

/***/ "next-server/dist/lib/router/router":
/*!*****************************************************!*\
  !*** external "next-server/dist/lib/router/router" ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-server/dist/lib/router/router");

/***/ }),

/***/ "next-server/dist/lib/utils":
/*!*********************************************!*\
  !*** external "next-server/dist/lib/utils" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-server/dist/lib/utils");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "prop-types-exact":
/*!***********************************!*\
  !*** external "prop-types-exact" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types-exact");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "reactjs-popup":
/*!********************************!*\
  !*** external "reactjs-popup" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("reactjs-popup");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ }),

/***/ "url":
/*!**********************!*\
  !*** external "url" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("url");

/***/ })

/******/ });
//# sourceMappingURL=hello.js.map